﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Point n Click/Player Manager")]
public class PlayerDatabase : ScriptableObject {

	// Use this for initialization
    public List<Player> player;

    public Player GetPlayer(string name)
    {
        foreach (Player p in player)
        {
            if (p.name == name)
                return p;
        }
        Debug.LogError("No player found with name "+ name);
        return null;
    }

    public Player GetPlayer(int id)
    {
        for (int i = 0; i < player.Count; i++)
        {
            if(player[i].id == id)
                return player[i];
        }
        Debug.LogError("No player found with name " + name);
        return null;
    }

    public Player GetActivePlayer()
    {
        for (int i = 0; i < player.Count; i++)
        {
            if (player[i].active)
                return player[i];
        }
        Debug.LogError("No Active player");
        return null;
    }

}
