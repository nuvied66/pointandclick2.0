﻿using UnityEngine;
using System.Collections;

public class TestInteraction : Interaction {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public override void Interact()
    {
        GameManager.actionManger.PlaySpeech("Helo.......................",GameManager.currentPlayer, SpeechPosition.Middle, SpeakerPosition.Left, OnFinish);
        GameManager.actionManger.Inventory(InventoryActionType.Add, GameManager.GetInvItem(-1));
    }


    void OnFinish()
    {

        //GameManager.actionManger.PlaySpeech("Hi how are you....", GameManager.playerData.GetPlayer("Ahmad"),SpeechPosition.Top);
    }
}
