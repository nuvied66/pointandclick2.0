﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(PlayerDatabase))]
public class PlayerDataEditor : Editor {

    PlayerDatabase pData;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        pData = (PlayerDatabase)target;
        if (GUILayout.Button("Add Player"))
        {
            Player p = new Player();
            pData.player.Add(p);
        }
    }
}
