﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class VariableEditor : EditorWindow {
    public VariableManager varManager;
    public References refs;
    [MenuItem("PointnClick/Variable Editor")]
    static void Init()
    {
        VariableEditor window = (VariableEditor)EditorWindow.GetWindow(typeof(VariableEditor));
        window.Show();
        window.maxSize = new Vector2(400f, 600f);
    }

    void OnGUI()
    {
        if (!refs)
        {
            try
            {
                refs = (References)Resources.Load("References");
                varManager = refs.varManager;
            }
            catch
            {
                if (!varManager)
                {
                    Debug.LogError("No reference manager found in resource folder....");
                    return;
                }
            }
        }
        GUILayout.Label("Variable Manager",EditorStyles.largeLabel);
        varManager = (VariableManager)EditorGUILayout.ObjectField("Asset file :", varManager, typeof(VariableManager), false);
        EditorGUILayout.Separator();

        if (!varManager)
            return;

        varManager.ShowGui();
    }
}
