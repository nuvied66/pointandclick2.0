﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public class InventoryEditor : EditorWindow {
    public InvDatabase invData;
    public References refs;

    [MenuItem("PointnClick/InvEditor")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        InventoryEditor Window = (InventoryEditor)EditorWindow.GetWindow(typeof(InventoryEditor));
        Window.Show();
        Window.maxSize = new Vector2(400f,600f);
        
    }


    public void OnGUI()
    {
        if (!refs)
        {
            try
            {
                refs = (References)Resources.Load("References");
                invData = refs.inventoryManager;
            }
            catch
            {
                //throw new UnityException("No reference manager found in resource folder....");
                if (!invData)
                {
                    Debug.LogError("No reference manager found in resource folder....");
                    return;
                }
            }
        }
       // if (!invData)
            //invData = (InvDatabase)Resources.Load("InvDatabase");
        
        GUILayout.Label("Inventory Manager", EditorStyles.largeLabel);

        
//
        invData = (InvDatabase)EditorGUILayout.ObjectField("Asset File :",invData, typeof(InvDatabase), false);
//
       EditorGUILayout.Separator();

        if (!invData)
            return;
        
       invData.ShowGUI();
       
    }

}
