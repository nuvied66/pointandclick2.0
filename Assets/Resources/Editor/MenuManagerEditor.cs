﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MenuManagerEditor : EditorWindow {

    UIManager uiManager;
	// Use this for initialization
    public References refs;
    [MenuItem("PointnClick/Menu Editor")]
    static void Init()
    {
        MenuManagerEditor window = (MenuManagerEditor)EditorWindow.GetWindow(typeof(MenuManagerEditor));
        window.Show();
        window.maxSize = new Vector2(400f, 600f);
    }

    void OnGUI()
    {
        if (!refs)
        {
            try
            {
                refs = (References)Resources.Load("References");
                uiManager = refs.uiManager;
            }
            catch
            {
                if (!uiManager)
                {
                    Debug.LogError("No reference manager found in resource folder....");
                    return;
                }
            }
        }
        GUILayout.Label("Menu Manager", EditorStyles.largeLabel);
        uiManager = (UIManager)EditorGUILayout.ObjectField("Asset file :", uiManager, typeof(UIManager), false);
        EditorGUILayout.Separator();

        if (!uiManager)
            return;

        uiManager.ShowGUI();
    }
}
