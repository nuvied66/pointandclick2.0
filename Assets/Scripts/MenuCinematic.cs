﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MenuCinematic : MonoBehaviour
{
    public Transform title, target, menuGroup, menuTarget;
    public SpriteRenderer title_sr;
    public GameCamera targetCam, currentCam;

    // Use this for initialization
    void Start()
    {
	
        GameManager.state = States.Paused;
        GameManager.TurnOffMenu(GameManager.GetMenu("Inventory"), 0f);

        CameraFade.StartAlphaFade(Color.black, true, 4f, 1f);
        title.DOMove(target.position, 2f);
        title.DOScale(target.localScale, 2f);
        title_sr.DOFade(1f, 2f).OnComplete(OnDoneFade);

        Camera.main.DOOrthoSize(targetCam.orthographicSize, 4f);
        MainCamera.Instance.currentCam = targetCam;


    }

    void OnDoneFade()
    {
        menuGroup.DOMove(menuTarget.position, 1f).SetEase(Ease.OutBounce).OnComplete(OnDoneMove);
    }

    void OnDoneMove()
    {
        GameManager.state = States.Normal;
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }
}
