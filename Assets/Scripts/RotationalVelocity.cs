﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class RotationalVelocity : MonoBehaviour
{

	// Use this for initialization
	float previousRotation;
	AudioSource sound;
	bool startSound;

	float velocity;

	void Start ()
	{
        sound = GetComponent<AudioSource>();
		sound.volume = 0;
		sound.loop = true;
		sound.Play ();
		Invoke ("Init", 1f);
	}

	void Init ()
	{
		startSound = true;
	}

	// Update is called once per frame
	void FixedUpdate ()
	{

		//rigidbody2D.MoveRotation (Mathf.Lerp (rigidbody2D.rotation, rigidbody2D.rotation + 10f, Time.deltaTime));
		velocity = (GetComponent<Rigidbody2D>().rotation - previousRotation);
		previousRotation = GetComponent<Rigidbody2D>().rotation;



		
	}

	void Update ()
	{
		if (!startSound)
			return;

		if (Mathf.Abs (velocity) < .01) {
			sound.volume = 0;
		} else {
			sound.volume = Mathf.Abs (velocity) * .15f;
			sound.pitch = 2f + Mathf.Abs (velocity) * .05f;

		}
		

	}
}
