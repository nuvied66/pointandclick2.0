﻿using UnityEngine;
using System.Collections;

public class ChangeSprite : Interaction
{
    public GameObject go;
    public Sprite spr;

    // Use this for initialization
    public override void Interact()
    {
        if (go != null && spr != null)
            go.GetComponent<SpriteRenderer>().sprite = spr;        
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }
}
