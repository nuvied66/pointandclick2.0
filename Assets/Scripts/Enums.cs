﻿public enum ActionType
{
    camera,
    inventory,
    variable,
    scene
}
;
public enum States
{
    Normal,
    Paused
}
;
public enum VarType
{
    bools,
    ints,
    floats,
    strings
}
;

public enum CameraActionType
{
    Switch,
    FadeIn,
    FadeOut
}
;
public enum InventoryActionType
{
    Add,
    Remove,
    Check
}
;
public enum SceneActionType
{
    Change,
    Check
}
;
public enum VarActionType
{
    Check,
    Set
}
;

public delegate void cAction(cAction nexAction = null);

public enum TransformActionType { MoveTo, RotateTo, CopyMarker };

public enum SpriteFadeType { FadeIn, FadeOut};

public enum MenuType {Inventory, Speech, Button};

public enum SpeechPosition {Top, Down, Middle};

public enum SpeakerPosition {Left, Right, Default };