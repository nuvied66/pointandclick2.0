﻿using UnityEngine;
using System.Collections;
using System;

public class RotateAround : MonoBehaviour
{

    public Transform target, point, finalPoint;
    public AudioClip aClip2;
    public float fRadius = 3.0f;
    public bool pressed, spring, done;





    void OnMouseDown()
    {
        
        
        pressed = true;

        gameObject.GetComponent<SliderJoint2D>().enabled = false;
        gameObject.GetComponent<CircleCollider2D>().enabled = true;

        //gameObject.GetComponent<CircleCollider2D>().enabled = true;
        //obj1.GetComponent<Rigidbody2D>().isKinematic = true;
        //obj2.GetComponent<Rigidbody2D>().isKinematic = true;
    }

    void OnMouseUp()
    {
        pressed = false;

//        if (GetComponent<SliderJoint2D>().enabled)
//        {
//            GetComponent<AudioSource>().clip = aClip2;
//            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
//        }

        //gameObject.GetComponent<CircleCollider2D>().enabled = false;
//        if (transform.position == point.transform.position)
//        {
//            transform.position = finalPoint.transform.position;
//        }
    }

    void Update()
    {
        
        if (pressed)
        {
            Vector3 v3Pos = Camera.main.WorldToScreenPoint(target.position);
            v3Pos = Input.mousePosition - v3Pos;
            float angle = Mathf.Atan2(v3Pos.y, v3Pos.x) * Mathf.Rad2Deg;
            v3Pos = Quaternion.AngleAxis(angle, Vector3.forward) * (Vector3.right * fRadius);
            //transform.position = target.position + v3Pos;
            transform.position = target.position + v3Pos;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.name == "pacman-piece" ||
            other.gameObject.name == "moon-piece" ||
            other.gameObject.name == "diamond-piece")
            gameObject.GetComponent<RotateAround>().pressed = false;

        //gameObject.GetComponent<SliderJoint2D>().enabled = true;
        //print(other.gameObject.name);
    }

    void OnCollisionExit2D(Collision2D other)
    {
       
        //gameObject.GetComponent<RotateAround>().pressed = false;
        //gameObject.GetComponent<SliderJoint2D>().enabled = false;
        //print(other.gameObject.name);
    }

    void ColliderEnable()
    {
    }

    void ColliderDisable()
    {
    }


}