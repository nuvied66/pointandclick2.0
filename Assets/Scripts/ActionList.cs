﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ActionList : MonoBehaviour {

    public ActionType actionType;
    public InventoryActionType invActionType;
    public SceneActionType sceneActionType;
    public VarActionType varActionType;

    public CameraActionType camActionType;

    public List<Actions> actions;


	void Start () {

        //RunAction();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void RunAction()
    {
        //print("Running Actions :" +actionType);
        if (actionType == ActionType.camera)
        {
            RunCameraAction();
        }
    }

    private void RunCameraAction()
    {
        if (camActionType == CameraActionType.FadeIn)
        {
            CameraFade.StartAlphaFade(Color.black, true, .25f);
        }
    }

#if UNITY_EDITOR

    public void ProcessAction()
    {
        switch(actionType)
        {
            case ActionType.camera:
                camActionType = (CameraActionType)EditorGUILayout.EnumPopup("Camera Action :", camActionType);
                break;
            case ActionType.inventory:
                invActionType = (InventoryActionType)EditorGUILayout.EnumPopup("Inventory Action :", invActionType);
                break;
            case ActionType.scene:
                sceneActionType = (SceneActionType)EditorGUILayout.EnumPopup("Scene Action :", sceneActionType);
                break;
            case ActionType.variable:
                varActionType = (VarActionType)EditorGUILayout.EnumPopup("Variable Action :", varActionType);
                break;

            default:
                break;
        }
    }
#endif
}
