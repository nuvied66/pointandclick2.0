﻿using UnityEngine;
using System.Collections;

public class DeleteSaveGames : MonoBehaviour {

	// Use this for initialization
    void Start()
    {
        
    }
    public void Delete()
    {
        GameManager.PlaySpeech("Game has been reset.....");
        SaveSystem.DeleteSave();
        PlayerPrefs.DeleteAll();
        
    }
}
