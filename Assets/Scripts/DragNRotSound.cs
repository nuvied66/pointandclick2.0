﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class DragNRotSound : MonoBehaviour
{
    public float currentRotation, oldRotation, nowRotation, pitch, vol, value1, value2;
    public Vector3 EularAngles;
    public AudioClip aClip;
    public bool didItPlay;

    void Awake()
    {
        //GetComponent<AudioSource>().Stop();
    }

    void start()
    {
        this.GetComponent<AudioSource>().clip = aClip;
        this.GetComponent<AudioSource>().Stop();
    }
    	
    // Update is called once per frame
    void Update()
    {
        //Invoke("AssignRotation", .01f);
        currentRotation = transform.rotation.z;
        nowRotation = currentRotation - oldRotation; 
        //print(nowRotation);

        if (nowRotation > value1 || nowRotation < value2 && !this.GetComponent<AudioSource>().isPlaying)
        {     
            Invoke("AssignRotation", .01f);
            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
            //this.GetComponent<AudioSource>().Play();
            //GetComponent<AudioSource>().pitch += nowRotation;
            //GetComponent<AudioSource>().volume += nowRotation;
        }
        else
        {
            this.GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().pitch = pitch;
            GetComponent<AudioSource>().volume = vol;
        }
    }

    void AssignRotation()
    {
        oldRotation = currentRotation;
    }
}
