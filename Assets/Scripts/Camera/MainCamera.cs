﻿using UnityEngine;
using System.Collections;
using UnityEngineInternal;
using UnityEngine.SceneManagement;

public class MainCamera : MonoBehaviour
{
	
	// Use this for initialization
	static MainCamera i;
	public GameCamera currentCam, previousCam;

	void Awake ()
	{
		i = this;
	}

	void OnLevelWasLoaded (int lvl)
	{
		
	}

	void Start ()
	{

        if (currentCam == null)
            return;

		//CameraFade.StartAlphaFade (Color.black, true, .5f);
		transform.position = currentCam.position;
		transform.rotation = currentCam.rotation;
		GetComponent<Camera> ().orthographicSize = currentCam.orthographicSize;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

    public static MainCamera Instance { get{return i;}}





}
