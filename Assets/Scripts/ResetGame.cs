﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class ResetGame : Interaction
{
    public override void Interact()
    {
        GlobalVaribalManager gVars = (GlobalVaribalManager)Resources.Load("Global Varibal Manager");
        InvDatabase iDB = (InvDatabase)Resources.Load("InvDatabase");

        foreach (Item i in iDB.items)
        {

            i.picked = false;
            i.used = false;
        }

        foreach (BoolGlobalVars v in gVars.boolVars)
        {
            v.value = false;
        }

        GameManager.inventory.items.Clear();

        //PlayerPrefs.SetString("sceneName", "BoatScene");  

       
        Debug.Log("Game has been reset......");
    }
}