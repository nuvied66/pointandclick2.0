﻿using UnityEngine;
using System.Collections;

public class TouchDrag : MonoBehaviour
{
	bool holded = false;
	Vector2 Pos;
	Vector2 offset;
	public float speed = 10f;
	public float SnapValu = .1f;
	Rigidbody2D rb;
	public int force;


	void Start ()
	{
		
		rb = GetComponent<Rigidbody2D> ();

		offset = transform.position;
	}

	void FixedUpdate ()
	{
		if (holded)
			return;


		rb.velocity = new Vector2 (force, 0);


	}
	// Use this for initialization
	void OnTouchDown (Vector2 position)
	{
		holded = true;
		offset = (Vector2)transform.position - position;

	}

	void OnTouchUp (Vector2 position)
	{
		
		holded = false;
		GetComponent<Rigidbody2D> ().position = MathC.SnapRound (GetComponent<Rigidbody2D> ().position, SnapValu);
	}

	void OnTouchStay (Vector2 position)
	{
		
		if (holded)
		{
			
			//Vector3 MousePos3d = Camera.main.ScreenToWorldPoint (new Vector3 (position.x, position.y, 10f));


			Vector2 MousePos2d = position + offset;

			Pos = Vector2.Lerp (GetComponent<Rigidbody2D> ().position, MousePos2d, Time.deltaTime * speed);

			GetComponent<Rigidbody2D> ().MovePosition (Pos);
	
		}

	}

	void OnTouchCanceled (Vector2 position)
	{
		
		holded = false;
		GetComponent<Rigidbody2D> ().position = MathC.SnapRound (GetComponent<Rigidbody2D> ().position, SnapValu);
	}


}
