﻿using UnityEngine;
using System.Collections;

public class DoorKnobScript : MonoBehaviour
{

    Rigidbody2D rb;
    public Rigidbody2D key;
    bool done;
    public Interaction i, j;
    //public AudioClip aClip;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
	
    }
	
    // Update is called once per frame
    void Update()
    {
        if (done)
            return;
		
        if (rb.rotation < -270 && key.rotation < -260)
        {
            Invoke("DonePuzzle", 1f);
            GetComponent<RotateWithMouse>().backForce = 0;
            key.GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
            done = true;
        }
    }

    void DonePuzzle()
    {
//        GetComponent<AudioSource>().clip = aClip;
//        GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
        i.Interact();
        j.Interact();

    }


}
