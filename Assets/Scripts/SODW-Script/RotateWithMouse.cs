﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


[RequireComponent(typeof(Rigidbody2D))]
public class RotateWithMouse : MonoBehaviour
{
    bool backSpring = true;

    public float backForce = 0;
    // Use this for initialization
    private float baseAngle = 0.0f;
    public float angleSnape = 10f;
    public string Message = "";

    Rigidbody2D rigidbody2D;


    void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        rigidbody2D.gravityScale = 0;
    }

	
    protected void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        if (Input.touchCount > 0 && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            return;
			
        Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
        pos = Input.mousePosition - pos;
        baseAngle = Mathf.Atan2(pos.y, pos.x) * Mathf.Rad2Deg;
        baseAngle -= Mathf.Atan2(transform.right.y, transform.right.x) * Mathf.Rad2Deg;
        backSpring = false;
    }

    protected void OnMouseUp()
    {
        rigidbody2D.MoveRotation(Mathf.Round(rigidbody2D.rotation / angleSnape) * angleSnape);

        backSpring = true;
        if (Message.Length > 0)
            SendMessage(Message, SendMessageOptions.DontRequireReceiver);

        rigidbody2D.angularVelocity = 0;


    }



    void LateUpdate()
    {


        if (!backSpring)
            return;

        rigidbody2D.AddTorque(backForce);
        //print (rigidbody2D.rotation);
    }
}
