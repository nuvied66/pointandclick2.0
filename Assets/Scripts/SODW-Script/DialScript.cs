﻿using UnityEngine;
using System.Collections;


public class DialScript : MonoBehaviour
{

    // Use this for initialization
    Transform pin;
    PinScript ppin;
    public Interaction interaction, j;
    //public AudioClip aClip;

    void Awake()
    {
		
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }

    void CheckPuzzle()
    {
        GameObject[] obj = GameObject.FindGameObjectsWithTag("Pin");
        for (int i = 0; i < obj.Length; i++)
        {
            if (!obj[i].GetComponent<PinScript>().Slided)
                return;
        }
        // do finish puzzle here
        interaction.Interact();
        j.Interact();

//        GetComponent<AudioSource>().clip = aClip;
//        GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);

    }


    void OnTriggerEnter2D(Collider2D col)
    {
        ppin = col.GetComponent<PinScript>();

        if (ppin.Slided)
            return;
		
        if (ppin.previousPin == pin || ppin.previousPin == null)
        {
            //print (col.GetComponent<PinScript> ().previousPin + " : " + pin);
           
            col.transform.Translate(new Vector3(0, .2f, 0));
            ppin.Slided = true;
            pin = col.transform;
            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
            CheckPuzzle();

        }
        else
        {
            print("wrong");
            GameObject[] obj = GameObject.FindGameObjectsWithTag("Pin");
            foreach (GameObject o in obj)
            {
				
                o.GetComponent<PinScript>().ResetPuzzle();
            }

        }
    }

    //	void OnTriggerExit2D (Collider2D col)
    //	{
    //
    //		if (col.GetComponent<PinScript> ().Slided)
    //			return;
    //
    //	}
}
