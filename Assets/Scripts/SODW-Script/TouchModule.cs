﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchModule : MonoBehaviour
{
	List<GameObject> touchList = new List<GameObject> ();
	GameObject[] oldTouchs;
	RaycastHit2D hit;

	public LayerMask mask;

	void Update ()
	{
		#if UNITY_EDITOR
		if (Input.GetMouseButton (0) || Input.GetMouseButtonDown (0) || Input.GetMouseButtonUp (0))
		{
			oldTouchs = new GameObject[touchList.Count];
			touchList.CopyTo (oldTouchs);
			touchList.Clear ();
	

			//Ray2D ray = Camera.main.ScreenToWorldPoint (t.position);
			hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), -Vector2.zero, 100f, mask);
			if (hit.collider == null)
				return;


			GameObject recipient = hit.collider.gameObject;

			touchList.Add (recipient);
			if (Input.GetMouseButtonDown (0))
			{
				recipient.SendMessage ("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
			}
			if (Input.GetMouseButtonUp (0))
			{
				recipient.SendMessage ("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
			}
			if (Input.GetMouseButton (0))
			{
				recipient.SendMessage ("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);
			}
		}

		#endif


		if (Input.touchCount > 0)
		{

			oldTouchs = new GameObject[touchList.Count];
			touchList.CopyTo (oldTouchs);
			touchList.Clear ();
			foreach (Touch t in Input.touches)
			{
				if (t.phase == TouchPhase.Ended)
				{
					SendMessage ("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
				}
				//Ray2D ray = Camera.main.ScreenToWorldPoint (t.position);
				hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (t.position), -Vector2.zero, Mathf.Infinity, mask);

				if (hit.collider == null)
					return;
				GameObject recipient = hit.collider.gameObject;

				touchList.Add (recipient);

//				print (touchList.Count);
//				if (touchList.Count > 1)
//					print (touchList [0].gameObject.name + " : " + touchList [1].gameObject.name);


				if (t.phase == TouchPhase.Began)
				{
					recipient.SendMessage ("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
				}
				if (t.phase == TouchPhase.Ended)
				{
					recipient.SendMessage ("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
				}
				if (t.phase == TouchPhase.Stationary || t.phase == TouchPhase.Moved)
				{
					recipient.SendMessage ("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);
				}
				if (t.phase == TouchPhase.Canceled)
				{
					recipient.SendMessage ("OnTouchCanceled", hit.point, SendMessageOptions.DontRequireReceiver);
				}


			}
			foreach (GameObject g in oldTouchs)
			{
				if (!touchList.Contains (g))
					g.SendMessage ("OnTouchCanceled", hit.point, SendMessageOptions.DontRequireReceiver);
			}

		}

	}

}