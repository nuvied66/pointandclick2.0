﻿using UnityEngine;
using System.Collections;

public class TriggerObject : MonoBehaviour
{

	// Use this for initialization
	CircleCollider2D[] c2d;
	void Start ()
	{
		c2d = GetComponents<CircleCollider2D> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		

//
		if (Input.GetMouseButtonUp (0)) {
			if (MoveableAngle ()) {
				c2d [0].isTrigger = true;
//				
			} else {
				c2d [0].isTrigger = false;

			}
		}

	
	}


//	void OnTriggerEnter2D (Collider2D other)
//	{
//
//
//		Debug.Log ("Enter");
//		other.GetComponent<SliderJoint2D> ().connectedBody = rigidbody2D;
//	}

	void OnTriggerExit2D (Collider2D other)
	{
		
		//		Debug.Log (other.name);
		//		other.transform.SetParent (transform);
		//		other.rigidbody2D.isKinematic = true;
		//other.GetComponent<CircleCollider2D> ().enabled = false;
		
		//other.GetComponent<SliderJoint2D> ().connectedBody = null;
		//Debug.Log ("Exit");
	}

	public bool MoveableAngle ()
	{

		if (MathC.SnapRound (transform.eulerAngles.z) == 0 ||
			MathC.SnapRound (transform.eulerAngles.z) == 60 ||
			MathC.SnapRound (transform.eulerAngles.z) == 120 ||
			MathC.SnapRound (transform.eulerAngles.z) == 180 ||
			MathC.SnapRound (transform.eulerAngles.z) == 240 ||
			MathC.SnapRound (transform.eulerAngles.z) == 300 ||
			MathC.SnapRound (transform.eulerAngles.z) == 360 
			    ) {
			return true;
		}
		return false;



	}
}
