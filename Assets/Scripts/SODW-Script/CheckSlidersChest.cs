﻿using UnityEngine;
using System.Collections;


public class CheckSlidersChest : MonoBehaviour
{

    public Interaction i;
    public SliderJoint2D s1, s2;
    bool done;
    // Use this for initialization
    void Start()
    {
	
    }
	
    // Update is called once per frame
    void FixedUpdate()
    {
        if (done)
            return;


        if (s2.jointTranslation > .45f && s1.jointTranslation < -.45f)
        {
			
            done = true;
            Invoke("RunAction", 1f);
            s1.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            s2.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            s1.GetComponent<TouchDrag>().force = 0;
            s2.GetComponent<TouchDrag>().force = 0;
        }
    }

    void RunAction()
    {
        i.Interact();
    }
}
