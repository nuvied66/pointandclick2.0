﻿using UnityEngine;
using System.Collections;
using System;

public class InitPins : MonoBehaviour
{

	// Use this for initialization
	public GameObject gameobj;


	void Start ()
	{
		float angle = 195;
		for (int i = 0; i < 12; i++)
		{


			GameObject go = GameObject.Instantiate (gameobj);
			go.transform.rotation = Quaternion.Euler (0, 0, angle);
			angle += 30;

		}

	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
