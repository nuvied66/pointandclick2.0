﻿using UnityEngine;
using System.Collections;

public class MathC
{
	



	public static float SnapRound (float v, float snap = 5f)
	{
		return Mathf.Round (v / snap) * snap;
	}

	public static Vector2 SnapRound (Vector2 v, float snapValu = .1f)
	{

		return new Vector2 (Mathf.Round (v.x / snapValu) * snapValu, Mathf.Round (v.y / snapValu) * snapValu);
	}


}
