﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class CheckChestPuzzle : MonoBehaviour
{
    public bool done;
    // Use this for initialization
    public Transform tr1, tr2, tr3;
    public Interaction interaction;


    public void CheckPuzzle()
    {
        //Do stuff here
        if (done)
            return;
       
        print(MathC.SnapRound(tr2.transform.rotation.eulerAngles.z));

        if ((MathC.SnapRound(tr1.transform.rotation.eulerAngles.z) == 5 &&
           MathC.SnapRound(tr2.transform.rotation.eulerAngles.z) == 5 &&
           MathC.SnapRound(tr3.transform.rotation.eulerAngles.z) == 5) ||
           MathC.SnapRound(tr1.transform.rotation.eulerAngles.z) == 365 &&
           MathC.SnapRound(tr2.transform.rotation.eulerAngles.z) == 365 &&
           MathC.SnapRound(tr3.transform.rotation.eulerAngles.z) == 365)
        {
            done = true;
            tr1.GetComponent<CircleCollider2D>().enabled = false;
            tr2.GetComponent<CircleCollider2D>().enabled = false;
            tr3.GetComponent<CircleCollider2D>().enabled = false;
            interaction.Invoke("Interact", 1f);

            
        }


    }

    void Update()
    {
        // print(MathC.SnapRound(tr[i].rotation.eulerAngles.z, 5f));
    }
}
