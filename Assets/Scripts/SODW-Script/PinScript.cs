﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using DG.Tweening.Plugins.Core.PathCore;

public class PinScript : MonoBehaviour
{
    public Transform previousPin;
    // Use this for initialization
    Vector3 orignalPos;

    bool _slided;

    public bool Slided
    {
        get
        {
            return _slided;
        }
        set
        {
            _slided = value;
        }
    }

    void Start()
    {

		
        orignalPos = transform.position;
    }

    public void ResetPuzzle()
    {
        if (Slided)
        {
            transform.position = orignalPos;
            Slided = false;
            //transform.Translate (new Vector3 (0, -.2f, 0));
            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
        }
    }

}
