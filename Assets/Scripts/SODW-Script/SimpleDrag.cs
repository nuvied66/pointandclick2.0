﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SimpleDrag : MonoBehaviour
{

    bool holded = false;
    Vector2 Pos;
    Vector2 offset;
    public float speed = 10f;
    public float SnapValu = .1f;



    // Use this for initialization
    void Start()
    {
        offset = transform.position;
    }
	
    // Update is called once per frame
    public void FixedUpdate()
    {
        #if UNITY_EDITOR
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        #endif
		
        if (holded)
        {
            Vector3 MousePos3d = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));
					
            Vector2 MousePos2d = new Vector2(MousePos3d.x, MousePos3d.y) + offset;
					
            Pos = Vector2.Lerp(GetComponent<Rigidbody2D>().position, MousePos2d, Time.deltaTime * speed);

            GetComponent<Rigidbody2D>().MovePosition(Pos);
            //float ang = (Mathf.Atan2 (MousePos2d.y, MousePos2d.x) * Mathf.Rad2Deg) * speed;

            //rigidbody2D.rotation = ang;
            //Debug.Log (rigidbody2D.rotation);
        }
    }

    protected void OnMouseDown()
    {
        holded = true;
        //offsetAngle = rigidbody2D.rotation;
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));
    }

    void OnMouseUp()
    {
        holded = false;

        GetComponent<Rigidbody2D>().position = MathC.SnapRound(GetComponent<Rigidbody2D>().position, SnapValu);
    }

    void OnMouseDrag()
    {
			
    }

}
