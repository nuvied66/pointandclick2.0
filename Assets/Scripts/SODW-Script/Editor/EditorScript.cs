﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.SceneManagement;

public class EditorScript : EditorWindow
{

    [MenuItem("PointnClick/MyWindow")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        EditorScript Window = (EditorScript)EditorWindow.GetWindow(typeof(EditorScript));
        Window.Show();
    }

    public void OnGUI()
    {

        if (GUI.Button(new Rect(10, 10, 100, 30), "Reset"))
        {

           
            References refs = (References)Resources.Load("References");
            VariableManager gVars = refs.varManager;
            InvDatabase iDB = refs.inventoryManager;

            if (GameManager.runtimeInventory != null)
            {
                GameManager.runtimeInventory.Clear();
            }
            foreach (Item i in iDB.items)
            {

                i.picked = false;
                i.used = false;

            }

            foreach (Variable v in gVars.vars)
            {

                v.boolValue = false;

            }

            PlayerPrefs.SetString("sceneName", "BoatScene");

            //PlayerPrefs.set

            Debug.Log("Game has been reset......");

        }



    }
}
