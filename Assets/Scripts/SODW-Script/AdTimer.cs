﻿using UnityEngine;
using System.Collections;

public class AdTimer : MonoBehaviour {

	// Use this for initialization

	public static AdTimer at;
	float timer;
	bool timeStart;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		timer += Time.deltaTime;

		if (timer > 180) {
			timer = 0;
			GameObject.FindGameObjectWithTag("AdObject").SendMessage("ShowAd");
			//Debug.Log("Send msg to ad obj");
		}

	
	}

	public void ResetTimer()
	{
		timer = 0;
	}

	void Awake ()
	{
		if (!at) {
			at = this;
			DontDestroyOnLoad (gameObject);
		} else {
			
			Destroy (gameObject);
		}
		
	}
}
