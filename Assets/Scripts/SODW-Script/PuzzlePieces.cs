﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PuzzlePieces : MonoBehaviour
{

    // Use this for initialization
    Rigidbody2D rb;
    public CheckChestPuzzle puzzleCheck;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.rotation = UnityEngine.Random.Range(0, 360);
	
    }
	
    // Update is called once per frame

    void Update()
    {    
            
        if (Input.GetMouseButtonUp(0))
            puzzleCheck.CheckPuzzle();
    }

}
