﻿using UnityEngine;
using System.Collections;

public class ShadowScript : MonoBehaviour
{

	// Use this for initialization

	Vector3 offset;
	public Transform t; 
	void Start ()
	{
		offset = transform.position - t.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.rotation = t.rotation;



		transform.position = t.position + offset;
	
	}
}
