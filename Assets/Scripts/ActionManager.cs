﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class ActionManager : MonoBehaviour
{

    //private static ActionManager _actionManger;

    //public static ActionManager Instance{ get { return _actionManger; } }

    int index;

    public void Awake()
    {
        /*
        if (!_actionManger)
        {
            _actionManger = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);  
        }
            
            */
    }

    void Start()
    {
	
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }

    public void CameraAction(CameraActionType type, float _duration = .25f, Action OnFinish = null)
    {
        switch (type)
        {
            case CameraActionType.FadeIn:
                CameraFade.StartAlphaFade(Color.black, true, _duration, OnFinish);
                break;
            case CameraActionType.FadeOut:
                CameraFade.StartAlphaFade(Color.black, false, _duration, OnFinish);
                break;
            case CameraActionType.Switch:
                Debug.LogError("Use CameraSwitch Methode");
                break;
        }
        
    }

    public void CameraAction(CameraActionType type, Action OnFinish = null)
    {
        switch (type)
        {
            case CameraActionType.FadeIn:
                CameraFade.StartAlphaFade(Color.black, true, 0, OnFinish);
                break;
            case CameraActionType.FadeOut:
                CameraFade.StartAlphaFade(Color.black, false, 0, OnFinish);
                break;
            case CameraActionType.Switch:
                Debug.LogError("Use CameraSwitch Methode");
                break;
        }

    }

    public void CameraSwitch(GameCamera _target, float _duration, TweenCallback OnFinish = null)
    {
        Camera c = Camera.main;
        if (c.GetComponent<MainCamera>() == null)
            Debug.LogError("Add MainCamera component on Main Camera");

        c.transform.DOMove(_target.transform.position, _duration);
        c.transform.DORotate(_target.transform.rotation.eulerAngles, _duration);
        MainCamera.Instance.previousCam = MainCamera.Instance.currentCam;
        MainCamera.Instance.currentCam = _target;
        if (OnFinish == null)
        {
            c.DOOrthoSize(_target.orthographicSize, _duration);
        }
        else
        {
            c.DOOrthoSize(_target.orthographicSize, _duration).OnComplete(OnFinish);
        }
    }

    public void CameraSwitch(GameCamera _target, TweenCallback OnFinish = null)
    {
        Camera c = Camera.main;

        c.transform.DOMove(_target.transform.position, 0);
        c.transform.DORotate(_target.transform.rotation.eulerAngles, 0);
        if (OnFinish == null)
        {
            c.DOOrthoSize(_target.orthographicSize, 0);
        }
        else
        {
            c.DOOrthoSize(_target.orthographicSize, 0).OnComplete(OnFinish);
        }
    }

    public bool VariableCheck(string boolVarName, Action OnFinish = null)
    {
        if (GameManager.varManager.GetBool(boolVarName))
        {
            return true;
        }
        return false;
    }



    public void Inventory(InventoryActionType type, Item _item, Action OnFinish = null)
    {
        switch (type)
        {
            case InventoryActionType.Add:
                GameManager.inventory.AddItem(_item.ID);
                if (OnFinish != null)
                    OnFinish.Invoke();
                break;
            case InventoryActionType.Remove:
                GameManager.inventory.RemoveItem(_item.ID);
                if (OnFinish != null)
                    OnFinish.Invoke();
                break;

        }
    }

    public void Inventory(InventoryActionType type, int _itemID, Action OnFinish = null)
    {
        switch (type)
        {
            case InventoryActionType.Add:
                GameManager.inventory.AddItem(_itemID);
                if (OnFinish != null)
                    OnFinish.Invoke();
                break;
            case InventoryActionType.Remove:
                GameManager.inventory.RemoveItem(_itemID);
                if (OnFinish != null)
                    OnFinish.Invoke();
                break;

        }
    }

    public void SceneSwitch(int idx)
    {
        index = idx;
        CameraAction(CameraActionType.FadeOut, LoadLevel);
    }

    public void Pause(float _duration, Action OnFinish)
    {
        GameManager.state = States.Paused;
        StartCoroutine(PauseRoutine(_duration, OnFinish));
    }

    public void TransformAction(TransformActionType type,Transform t, Transform _target, float _duration, TweenCallback OnFinish = null)
    {
        switch (type)
        {
            case TransformActionType.MoveTo:
                {
                    if (OnFinish != null)
                        t.DOMove(_target.position, _duration).OnComplete(OnFinish);
                    else
                        t.DOMove(_target.position, _duration);
                    break;
                }
            case TransformActionType.RotateTo:
                {
                    if (OnFinish != null)
                        t.DORotate(_target.eulerAngles, _duration).OnComplete(OnFinish);
                    else
                        t.DORotate(_target.eulerAngles, _duration);
                    
                    break;
                }
            case TransformActionType.CopyMarker:
                {
                    if (OnFinish != null)
                    {
                        t.DOMove(_target.position, _duration);
                        t.DORotate(_target.eulerAngles, _duration).OnComplete(OnFinish);
                    }
                    else
                    {
                        t.DOMove(_target.position, _duration);
                        t.DORotate(_target.eulerAngles, _duration);
                    }

                    break;
                }

        }
    }

    public void TransformAction(TransformActionType type, Transform t, Transform _target, float _duration,Ease ease, TweenCallback OnFinish = null)
    {
        switch (type)
        {
            case TransformActionType.MoveTo:
                {
                    if (OnFinish != null)
                        t.DOMove(_target.position, _duration).SetEase(ease).OnComplete(OnFinish);
                    else
                        t.DOMove(_target.position, _duration).SetEase(ease);
                    break;
                }
            case TransformActionType.RotateTo:
                {
                    if (OnFinish != null)
                        t.DORotate(_target.eulerAngles, _duration).SetEase(ease).OnComplete(OnFinish);
                    else
                        t.DORotate(_target.eulerAngles, _duration).SetEase(ease);

                    break;
                }
            case TransformActionType.CopyMarker:
                {
                    if (OnFinish != null)
                    {
                        t.DOMove(_target.position, _duration);
                        t.DORotate(_target.eulerAngles, _duration).SetEase(ease).OnComplete(OnFinish);
                    }
                    else
                    {
                        t.DOMove(_target.position, _duration);
                        t.DORotate(_target.eulerAngles, _duration).SetEase(ease);
                    }

                    break;
                }

        }
    }

    public void SpriteFade(SpriteFadeType type,SpriteRenderer _sr, float _duration, TweenCallback OnFinish = null)
    {
        if (type == SpriteFadeType.FadeIn)
        {
            if(OnFinish == null)
                _sr.DOFade(1, _duration);
            else
                _sr.DOFade(1, _duration).OnComplete(OnFinish);
        }
        else if(type == SpriteFadeType.FadeOut)
        {
            if (OnFinish == null)
                _sr.DOFade(0, _duration);
            else
                _sr.DOFade(0, _duration).OnComplete(OnFinish);

        }
    }

    public void SpriteFade(SpriteFadeType type, SpriteRenderer _sr, float _duration,Ease ease, TweenCallback OnFinish = null)
    {
        if (type == SpriteFadeType.FadeIn)
        {
            if (OnFinish == null)
                _sr.DOFade(1, _duration).SetEase(ease);
            else
                _sr.DOFade(1, _duration).SetEase(ease).OnComplete(OnFinish); 
        }
        else if (type == SpriteFadeType.FadeOut)
        {
            if (OnFinish == null)
                _sr.DOFade(0, _duration).SetEase(ease);
            else
                _sr.DOFade(0, _duration).SetEase(ease).OnComplete(OnFinish);

        }
    }

    public void PlaySpeech(string _speech,bool typein, Action OnFinish = null)
    {
        if (_speech.Length <= 0 || GameManager.IsOn(GameManager.GetMenu("Speech")))
            return;

        GameManager.state = States.Paused;
        GameManager.i.StopAllCoroutines();
        GameManager.i.CancelInvoke();
        //letter.Clear ();
        
        GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().text.text = "";
        GameManager.TurnOnMenu(GameManager.GetMenu("Speech"));
        GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().GetComponent<RectTransform>().DOAnchorPosX(-80, 0f);
        DisableSpeaker();
        //GameObject.FindGameObjectWithTag ("Dialogue").GetComponentInChildren<UnityEngine.UI.Text> ().text = "";
        //GameObject.FindGameObjectWithTag ("Dialogue").GetComponent<Canvas> ().enabled = true;
        GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().text.color = GameManager.currentPlayer.letterColor;
        GameManager.GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosY(0, .1f);
        if (!typein)
        {

            GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().text.text = _speech;
        }
        else
        {
            StartCoroutine(TypeText(_speech));
        }

        StartCoroutine(DisableSpeechUI(_speech.Length / 5, OnFinish));
        
    }

    public void PlaySpeech(string _speech, bool typein,SpeechPosition pos, Action OnFinish = null)
    {
        if (_speech.Length <= 0 || GameManager.IsOn(GameManager.GetMenu("Speech")))
            return;

        GameManager.state = States.Paused;
        GameManager.i.StopAllCoroutines();
        GameManager.i.CancelInvoke();
        //letter.Clear ();
        GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().GetComponent<RectTransform>().DOAnchorPosX(-80, 0f);
        DisableSpeaker();
        GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().text.text = "";
        
        GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().text.color = GameManager.currentPlayer.letterColor;
        GameManager.TurnOnMenu(GameManager.GetMenu("Speech"));

        //GameObject.FindGameObjectWithTag ("Dialogue").GetComponentInChildren<UnityEngine.UI.Text> ().text = "";
        //GameObject.FindGameObjectWithTag ("Dialogue").GetComponent<Canvas> ().enabled = true;
        if(pos == SpeechPosition.Down)
            GameManager.GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosY(-170,.1f);
        else if(pos == SpeechPosition.Top)
            GameManager.GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosY(70, .1f);

        if (!typein)
        {

            GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().text.text = _speech;
        }
        else
        {
            StartCoroutine(TypeText(_speech));
        }

        //Invoke("DisableSpeechUI", _speech.Length / 5f);
        StartCoroutine(DisableSpeechUI(_speech.Length / 5, OnFinish));

    }

    public void PlaySpeech(string _speech, Player speaker, SpeechPosition pos, Action OnFinish = null)
    {
    	if (_speech.Length <= 0 || GameManager.IsOn(GameManager.GetMenu("Speech")))
            return;

        GameManager.state = States.Paused;
        GameManager.i.StopAllCoroutines();
        GameManager.i.CancelInvoke();

        EnableSpeaker(speaker);
        UnityEngine.UI.Text uiText;
        uiText = GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().text;
        uiText.text = "";
        uiText.color = speaker.letterColor;

        GameManager.TurnOnMenu(GameManager.GetMenu("Speech"));

        if (pos == SpeechPosition.Down)
            GameManager.GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosY(-170, .1f);
        else if (pos == SpeechPosition.Top)
            GameManager.GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosY(70, .1f);

        StartCoroutine(TypeText(_speech));

        StartCoroutine(DisableSpeechUI(_speech.Length / 5, OnFinish));

    }

    public void PlaySpeech(string _speech, Player speaker, SpeechPosition pos, SpeakerPosition spos, Action OnFinish = null)
    {
        if (_speech.Length <= 0 || GameManager.IsOn(GameManager.GetMenu("Speech")))
            return;

        GameManager.state = States.Paused;
        GameManager.i.StopAllCoroutines();
        GameManager.i.CancelInvoke();

        EnableSpeaker(speaker, spos);
        UnityEngine.UI.Text uiText;
        uiText = GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().text;
        uiText.text = "";
        uiText.color = speaker.letterColor;

        GameManager.TurnOnMenu(GameManager.GetMenu("Speech"));

        if (pos == SpeechPosition.Down)
            GameManager.GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosY(-170, .1f);
        else if (pos == SpeechPosition.Top)
            GameManager.GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosY(70, .1f);

        StartCoroutine(TypeText(_speech));

        StartCoroutine(DisableSpeechUI(_speech.Length / 5, OnFinish));

    }

    void EnableSpeaker(Player player, SpeakerPosition spos = SpeakerPosition.Default)
    {
        if(spos == SpeakerPosition.Left)
        {
            GameManager.GetMenu("Speech").GetComponentInChildren<PlayerImage>().GetComponent<RectTransform>().DOAnchorPosX(-340, 0f); // varies according to menus
            GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().GetComponent<RectTransform>().DOAnchorPosX(80, 0f); // Varies according to menus
        }
        else if (spos == SpeakerPosition.Right)
        {
            GameManager.GetMenu("Speech").GetComponentInChildren<PlayerImage>().GetComponent<RectTransform>().DOAnchorPosX(340, 0f); // varies according to menus
            GameManager.GetMenu("Speech").GetComponentInChildren<DialogueLine>().GetComponent<RectTransform>().DOAnchorPosX(-80, 0f); // Varies according to menus
        }
        GameManager.GetMenu("Speech").GetComponentInChildren<PlayerImage>().playerImg.enabled = true;
        GameManager.GetMenu("Speech").GetComponentInChildren<PlayerImage>().playerImg.sprite = player.playerImage;
        GameManager.GetMenu("Speech").GetComponentInChildren<PlayerImage>().transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().name = player.name;
        GameManager.GetMenu("Speech").GetComponentInChildren<PlayerImage>().transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().enabled = true;
    }

    void DisableSpeaker()
    {
        GameManager.GetMenu("Speech").GetComponentInChildren<PlayerImage>().playerImg.enabled = false;
        GameManager.GetMenu("Speech").GetComponentInChildren<PlayerImage>().transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().enabled = false;
    }

    IEnumerator DisableSpeechUI(float _duration,Action OnFinish)
    {
        
        yield return new WaitForSeconds(_duration);

        StopAllCoroutines();
        GameManager.TurnOffMenu("Speech");
        GameManager.state = States.Normal;
        GameManager.GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosX(0, .1f);
        
        if(OnFinish != null)
             OnFinish.Invoke();
        
    }

    IEnumerator TypeText(string message)
    {
        //		foreach (char letter in message.ToCharArray()) {
        //			GetMenu ("Speech").GetComponentInChildren<UnityEngine.UI.Text> ().text += letter;
        float letterPause = .01f;
        char[] letter = message.ToCharArray();

        for (int i = 0; i < message.Length; i++)
        {
            GameManager.GetMenu("Speech").GetComponentInChildren<UnityEngine.UI.Text>().text += letter[i];




            yield return new WaitForSeconds(letterPause);
        }


    }

    IEnumerator PauseRoutine(float _duration, Action OnFinish)
    {
        float time = 0;
        while(time <= _duration)
        {
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        GameManager.state = States.Normal;
        OnFinish.Invoke();
        
    }

    private void LoadLevel()
    {
        SceneManager.LoadScene(index);
    }


}
