﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MenuInventory : Menu {

	// Use this for initialization
    public MenuInventory()
    {
        type = MenuType.Inventory;
    }
#if UNITY_EDITOR
    internal override void ShowGUI()
    {

    }
#endif
}
