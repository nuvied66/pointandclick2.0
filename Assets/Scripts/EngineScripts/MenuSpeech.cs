﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif


[System.Serializable]
public class MenuSpeech : Menu {

    public RectTransform parentRect;
    //public Text speechText;
    
    public MenuSpeech()
    {
        type = MenuType.Speech;
    }
    #if UNITY_EDITOR
    internal override void ShowGUI()
    {
        parentRect = (RectTransform)EditorGUILayout.ObjectField("Select Parent Rect :", parentRect, typeof(RectTransform));
        //speechText = (Text)EditorGUILayout.ObjectField("Select Text UI :", speechText, typeof(Text));
        
    }
#endif

    
}
