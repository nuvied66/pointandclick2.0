﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MixSlotScript : MonoBehaviour, IDropHandler {

	// Use this for initialization
    public Item slotItem;
	void Start () {

        slotItem = null;
	}
	
	// Update is called once per frame
	void Update () {
	
	}



    public void Combine(int ID1, int ID2, int resultID)
    {
        if (ID1 >= 0 || ID2 >= 0)
            return;

        //GameManager.inventory.RemoveItem (ID2);
        //GameManager.inventory.AddItem (resultID);
        //GameManager.inventory.ReplaceItem(ID2, resultID);


        if (GameManager.GetInvItem(ID1) != null)
        {
            if (!GameManager.GetInvItem(ID1).retainAfterCombine)
            {
                GameManager.GetInvItem(ID1).used = true;
                GameManager.inventory.RemoveItem(ID1);
            }
        }

        if (GameManager.GetInvItem(ID2) != null)
        {
            if (!GameManager.GetInvItem(ID2).retainAfterCombine)
            {
                GameManager.GetInvItem(ID2).used = true;
                GameManager.inventory.RemoveItem(ID2);
            }
        }

        if (GameManager.GetInvItem(resultID) != null)
        {
            GameManager.GetInvItem(resultID).picked = true;
            GameManager.inventory.AddItem(resultID);
        }







    }



    public void OnDrop(PointerEventData eventData)
    {
        if (GameManager.currentItem == null)
            return;
        if (slotItem == null)
        {
            GetComponent<Image>().color = Color.white;
            GetComponent<Image>().sprite = GameManager.currentItem.icon;
            slotItem = GameManager.currentItem;
            return;
        }


        if (GameManager.currentItem.combineID == slotItem.ID)
        {
            Combine(GameManager.currentItem.ID, slotItem.ID, slotItem.resultID);
            GameManager.PlaySpeech(slotItem.combineDiscription);
            GetComponent<Image>().color = Color.clear;
            slotItem = null;

        }
        else
        {
            GameManager.PlaySpeech("Cannot be combined");
            GetComponent<Image>().color = Color.clear;
            slotItem = null;
        }
    }
}
