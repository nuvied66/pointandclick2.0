﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.CodeDom.Compiler;


#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Point n Click/Inventory")]
public class InvDatabase : ScriptableObject
{
    public IconsDatabase iconDB;
    public List<Item> items;
    //List<Icon> icons;
    // Use this for initialization


#if UNITY_EDITOR
    int ID;
    string name;
    Sprite image;
    //string[] combineItems;
    int index = 0;
    bool selected;
    Item selectedItem;
    Vector2 scrollPos;
    List<string> itemName = new List<string>();
    int resultIdx = 0;
    string namefilter = "";
    

    public void ShowGUI()
    {
        //itemName.Clear();
        if (index > items.Count - 1)
            index = 0;
        if (resultIdx > items.Count - 1)
            resultIdx = 0;
        EditorGUILayout.Space();
        if (itemName.Count != items.Count)
        {
            itemName.Clear();

            foreach (Item i in items)
            {
                itemName.Add(i.name);

            }
        }


        //name = EditorGUILayout.TextField("Item Name :", name);
        //ID = EditorGUILayout.IntField("Item ID :",ID);
        //index = EditorGUILayout.Popup("Combine Item :",index,combineItems );
        //image = (Sprite)EditorGUILayout.ObjectField("Icon :",image,typeof(Sprite), false);
        //EditorGUILayout.Space();
        namefilter = EditorGUILayout.TextField("Search :", namefilter);

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Height(Mathf.Min(items.Count * 21, 235f) + 5));

        foreach (Item i in items.ToArray())
        {
            /////////////////////////////////////
            if (i.name.ToLower().Contains(namefilter.ToLower()) || namefilter == "")
            {
                GUILayout.BeginHorizontal();
                if (i.name == "")
                {
                    i.name = "Unnamed" + i.ID;
                }
                if (GUILayout.Toggle(i.isEditing, i.name + " : " + i.ID, "toolbarbutton"))
                {
                    if (selectedItem != i)
                    {
                        DeactivateAllItems();
                        i.isEditing = true;
                        selectedItem = i;
                    }
                }

                if (GUILayout.Button("Delete", GUILayout.Width(100f), GUILayout.Height(15f)))
                {
                    Undo.RecordObject(this, "delete item");
                    items.RemoveAt(items.IndexOf(i));
                    iconDB.RemoveIcon(i.ID);
                    DeactivateAllItems();
                }
                GUILayout.EndHorizontal();
                
            }
        }
        ////////////////////////////////////////////
            EditorGUILayout.EndScrollView();

        GUILayout.BeginHorizontal();
       

        if (GUILayout.Button("Add Item"))
        {
            //Undo.RecordObject(this,"AddItem");
            Item i = new Item();
            i.name = "";
            if (items.Count > 0)
                i.ID = items[items.Count - 1].ID - 1; // last items id - 1
            else
                i.ID = -1;

            items.Add(i);

            Icon ic = new Icon();
            ic.id = i.ID;
            iconDB.icons.Add(ic);
            itemName.Add(i.name);
            selectedItem = i;
        }

        GUILayout.EndHorizontal();

        ItemsGUI();
        //Debug.Log(index+ "," +resultIdx);
        if (GUI.changed)
        {
            EditorUtility.SetDirty(this);
        }
    }

    private void ItemsGUI()
    {
        if (selectedItem == null || !items.Contains(selectedItem))
            return;
        selectedItem.name = EditorGUILayout.TextField("Item Name :", selectedItem.name);
        EditorGUILayout.LabelField("Item Discription :");
        selectedItem.discription = EditorGUILayout.TextArea(selectedItem.discription,GUILayout.Height(30f));
        selectedItem.carryOnStart = EditorGUILayout.Toggle("Carry On Start ?:", selectedItem.carryOnStart);
        EditorGUILayout.Space();
        if(selectedItem.icon == null)
             selectedItem.icon = iconDB.AssignIcon(selectedItem);
        selectedItem.icon = (Sprite)EditorGUILayout.ObjectField("Icon :", selectedItem.icon, typeof(Sprite), false);
        

        selectedItem.canCombine = EditorGUILayout.BeginToggleGroup("Can Combine ? :", selectedItem.canCombine);

            //selectedItem.combineID = EditorGUILayout.IntField("Combine ID : ", selectedItem.combineID);
        if (selectedItem.canCombine)
        {
            index = IDToIndex(selectedItem.combineID);
            resultIdx = IDToIndex(selectedItem.resultID);

            items[index].canCombine = true;

            index = EditorGUILayout.Popup("Item to Combine :", index, itemName.ToArray());
            resultIdx = EditorGUILayout.Popup("Result item :", resultIdx, itemName.ToArray());
            //Debug.Log(index);

            selectedItem.combineDiscription = EditorGUILayout.TextArea(selectedItem.combineDiscription, GUILayout.Height(30f));
            items[index].combineDiscription = selectedItem.combineDiscription;

            selectedItem.combineID = items[index].ID;
            items[index].combineID = selectedItem.ID;
            selectedItem.resultID = items[resultIdx].ID;

            items[index].resultID = items[resultIdx].ID;

            selectedItem.retainAfterCombine = EditorGUILayout.Toggle("Keep After Combine ?:", selectedItem.retainAfterCombine);
        }
        else
        {
            selectedItem.combineID = 0;
            selectedItem.resultID = 0;
        }

              
        EditorGUILayout.EndToggleGroup();

        for(int i = 0; i < items.Count; i++)
        {
            itemName[i] = items[i].name+" : "+items[i].ID;
        }


        foreach(Icon i in iconDB.icons)
        {
            if (i.id == selectedItem.ID)
            {
                i.icon = selectedItem.icon;
                i.name = selectedItem.name;
            }
        }
    }

    private void DeactivateAllItems()
    {
        foreach (Item i in items)
        {
            i.isEditing = false;
        }
        selectedItem = null;
    }

 #endif

    public Item GetItemByName(string name)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].name == name)
                return items[i];
        }
        return null;
    }


    public Item GetItemByID(int ID)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].ID == ID)
                return items[i];
        }
        return null;
    }

    private int IDToIndex(int id)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].ID == id)
            {
                return i;
            }
        }
        return 0;
    }


}
