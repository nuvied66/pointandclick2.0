﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu (menuName = "Point n Click/UIManager")]
public class UIManager : ScriptableObject
{
	public GameObject cursor;
	public List<Menu> gUI;

	// Use this for initialization
    #if UNITY_EDITOR
    string nameFilter = "";
    Vector2 scrollPos = new Vector2(0,0);
    Menu selectedMenu;

    public void ShowGUI()
    {
        //GUILayout.Label("Variable Editor", EditorStyles.largeLabel);

        nameFilter = EditorGUILayout.TextField("Search : ?", nameFilter);

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Height(Mathf.Min(gUI.Count * 21, 235f) + 5));

        foreach (Menu v in gUI.ToArray())
        {
            if (v.name.ToLower().Contains(nameFilter.ToLower()) || nameFilter == "")
            {
                GUILayout.BeginHorizontal();
                if (v.name == "")
                {
                    v.name = "new"+v.type.ToString()+" "+ v.ID;
                }
                if (GUILayout.Toggle(v.isEditing, v.name + " : " + v.ID, "toolbarbutton"))
                {
                    if (selectedMenu != v)
                    {
                        DeactivateAllItems();
                        v.isEditing = true;
                        selectedMenu = v;
                    }
                }

                if (GUILayout.Button("Delete", GUILayout.Width(100f), GUILayout.Height(15f)))
                {
                    Undo.RecordObject(this, "delete item");
                    gUI.RemoveAt(gUI.IndexOf(v));
                    DeactivateAllItems();
                }
                GUILayout.EndHorizontal();

            }
        }

        EditorGUILayout.EndScrollView();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Add menu Item"))
        {/*
            Menu m = new Menu();
            m.name = "NewMenu";
            if (gUI.Count > 0)
                m.ID = gUI[gUI.Count - 1].ID + 1;
            else
                m.ID = 0;

            gUI.Add(m);
            selectedMenu = m;*/
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Menu Button"), false, CallBack, 0);
            menu.AddItem(new GUIContent("Menu Inventory"), false, CallBack, 1);
            menu.AddItem(new GUIContent("Menu Speech"), false, CallBack, 2);
            menu.ShowAsContext();

        }
        EditorGUILayout.EndHorizontal();

        MenuGUI();
        //Debug.Log(index+ "," +resultIdx);
        if (GUI.changed)
        {
            EditorUtility.SetDirty(this);
        }
    }

    private void CallBack(object userData)
    {
       // Debug.Log("user Data "+ userData);
        switch ((int)userData)
        {
            case 0:
                {
                    Menu m = ScriptableObject.CreateInstance<MenuButton>();
                    m.hideFlags = HideFlags.HideInHierarchy;
                    m.name = "NewButtonMenu";
                    if (gUI.Count > 0)
                        m.ID = gUI[gUI.Count - 1].ID + 1;
                    else
                        m.ID = 0;

                    gUI.Add(m);
                    selectedMenu = m;
                    SaveAsset(m);
                    break;
                }

            case 1:
                {
                    Menu m = ScriptableObject.CreateInstance<MenuInventory>();
                    m.hideFlags = HideFlags.HideInHierarchy;
                    m.name = "NewInvMenu";
                    if (gUI.Count > 0)
                        m.ID = gUI[gUI.Count - 1].ID + 1;
                    else
                        m.ID = 0;

                    gUI.Add(m);
                    selectedMenu = m;
                    SaveAsset(m);
                    break;
                    
                }

            case 2:
                {
                    Menu m = ScriptableObject.CreateInstance<MenuSpeech>();
                    m.hideFlags = HideFlags.HideInHierarchy;
                    m.name = "Speech";
                    if (gUI.Count > 0)
                        m.ID = gUI[gUI.Count - 1].ID + 1;
                    else
                        m.ID = 0;

                    gUI.Add(m);
                    selectedMenu = m;
                    SaveAsset(m);
                    
                    break;
                }
        }
    }

    private void SaveAsset(Menu m)
    {
        AssetDatabase.AddObjectToAsset(m, this);
        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(m));
        AssetDatabase.Refresh();
    }

    private void DeactivateAllItems()
    {
        foreach (Menu m in gUI)
        {
            m.isEditing = false;
        }
        selectedMenu = null;
    }

    private void MenuGUI()
    {
        if (selectedMenu == null || !gUI.Contains(selectedMenu))
            return;

        selectedMenu.name = EditorGUILayout.TextField("Menu Name :", selectedMenu.name);
        selectedMenu.canvas = (Canvas)EditorGUILayout.ObjectField("Assign Canvas :", selectedMenu.canvas, typeof(Canvas));
        selectedMenu.enableOnStart = EditorGUILayout.Toggle("Enable On Start", selectedMenu.enableOnStart);
        selectedMenu.ShowGUI();


        
    }
	public Menu GetMenuByName (string name)
	{
		for (int i = 0; i < gUI.Count; i++)
		{
			if (gUI [i].name == name)
			{
				return gUI [i];
			}
		}
		return null;
	}

#endif


    public MenuSpeech GetSpeechMenu()
    {
      foreach(Menu m in gUI)
      {
          if (m.type == MenuType.Speech)
              return m as MenuSpeech;
      }
      return null;
    }


    public MenuInventory GetInventoryMenu()
    {
        foreach (Menu m in gUI)
        {
            if (m.type == MenuType.Inventory)
                return m as MenuInventory;
        }
        return null;
    }

}
