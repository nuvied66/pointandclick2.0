﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Point n Click/Manager Refs")]
public class References : ScriptableObject {

    public UIManager uiManager;
    public InvDatabase inventoryManager;
    public VariableManager varManager;
    public PlayerDatabase playerData;
    //public IconsDatabase iconManager;
}
