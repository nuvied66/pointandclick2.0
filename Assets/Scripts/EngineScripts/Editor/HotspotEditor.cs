﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CustomEditor(typeof(Hotspot))]
public class HotspotEditor : Editor {

    Hotspot hotspot;
    public string name;
    public InvDatabase invData;
    public List<string> itemName = new List<string>();
    
#if UNITY_EDITOR
    public override void OnInspectorGUI()
    {
        if (invData == null)
        {
            References refs = (References)Resources.Load("References");
            invData = refs.inventoryManager;
        }
        hotspot = (Hotspot)target;
        if (itemName.Count != invData.items.Count)
        {
            itemName.Clear();

            foreach (Item item in invData.items)
            {
                itemName.Add(item.name);
            }
        }



        name = hotspot.gameObject.name;
        name = EditorGUILayout.TextField("Hotspot Name :",name);
        hotspot.gameObject.name = name;
        EditorGUILayout.Space();


        
        EditorGUILayout.BeginVertical("Button");
        GUILayout.Label("Assign Default Interaction Here", EditorStyles.boldLabel);
        hotspot.defaultInt = (Interaction)EditorGUILayout.ObjectField("Interaction :", hotspot.defaultInt,typeof(Interaction) );

        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        hotspot.hasItemInt = EditorGUILayout.BeginToggleGroup("Has an Item Interation ?:", hotspot.hasItemInt);
        EditorGUILayout.BeginVertical("Button");
        GUILayout.Label("Assign Item Interaction Here", EditorStyles.boldLabel);

        hotspot.itemIdx = EditorGUILayout.Popup("Select Interact Item :", hotspot.itemIdx,itemName.ToArray()); 
        hotspot.itemInt = (Interaction)EditorGUILayout.ObjectField("Interaction :", hotspot.itemInt, typeof(Interaction));
        hotspot.discardOnUse = EditorGUILayout.Toggle("Discard Item On Use ? :", hotspot.discardOnUse);

        EditorGUILayout.EndVertical();
        EditorGUILayout.EndToggleGroup();




        if (GUI.changed)
        {
            EditorUtility.SetDirty(target);
        }
    }

    GameObject CreateInteraction()
    {
        GameObject interaction = new GameObject(target.name + "_interaction");
        //interaction.AddComponent(typeof(Interaction));
        return interaction;
    }
#endif
}
