﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(ActionList))]
public class ActionListEditor :Editor  {

    ActionList actionList;
    public void OnEnable()
    {
        actionList = (ActionList)target;
    }
    public override void OnInspectorGUI()
    {
        actionList.actionType = (ActionType)EditorGUILayout.EnumPopup("Action Type :", actionList.actionType);
        actionList.ProcessAction();
    }
}
