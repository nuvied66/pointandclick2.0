﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HintText : MonoBehaviour
{

    // Use this for initialization
    public static HintText i;

    void Start()
    {
        i = this;
    }
	
    // Update is called once per frame
    void Update()
    {
	
    }

    public static void SetText(string text)
    {
        
        i.GetComponent<Text>().text = text;
    }
}

