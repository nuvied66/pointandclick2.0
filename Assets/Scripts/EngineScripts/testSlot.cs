﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class testSlot : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDropHandler{

	// Use this for initialization
    float counter = 0;
    bool pressed = false;
    bool picked = false;
    public Item slotItem;
    public ScrollRect scrollRect;

	void Start () {
        scrollRect = transform.parent.parent.parent.GetComponent<ScrollRect>();
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonUp(0))
        {
            picked = false;
            counter = 0;
            scrollRect.horizontal = true;
        }
        if (picked)
            return;


        if (pressed)
        {
            CursorUI.img.transform.position = Input.mousePosition;
            counter += Time.deltaTime;
            if (counter > .25f)
            {
                Viberation.Vibrate(20);
                //print("Item selected = " + slotItem.name);
                picked = true;
                GameManager.currentItem = slotItem;
                CursorUI.EnableCursor();
                scrollRect.horizontal = false;
                //transform.parent.parent.parent.GetComponent<ScrollRect>().enabled = false;


            }
        }
        
	
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        
        pressed = true;
        picked = false;
        
    }






    public void OnPointerUp(PointerEventData eventData)
    {
        
        pressed = false;
        picked = false;
        counter = 0;
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (GameManager.currentItem == null)
            return;

        if (GameManager.currentItem.combineID == slotItem.ID)
        {
            Combine(GameManager.currentItem.ID, slotItem.ID, slotItem.resultID);
            GameManager.PlaySpeech(slotItem.combineDiscription);
            SizeChange.Instance.UpateInventory();

        }
        else
        {
            GameManager.PlaySpeech("Cannot be combined");
        }
    }

    public void Combine(int ID1, int ID2, int resultID)
    {
        if (ID1 >= 0 || ID2 >= 0)
            return;

        //GameManager.inventory.RemoveItem (ID2);
        //GameManager.inventory.AddItem (resultID);
        //GameManager.inventory.ReplaceItem(ID2, resultID);


        if (GameManager.GetInvItem(ID1) != null)
        {
            if (!GameManager.GetInvItem(ID1).retainAfterCombine)
            {
                GameManager.GetInvItem(ID1).used = true;
                GameManager.inventory.RemoveItem(ID1);
            }
        }

        if (GameManager.GetInvItem(ID2) != null)
        {
            if (!GameManager.GetInvItem(ID2).retainAfterCombine)
            {
                GameManager.GetInvItem(ID2).used = true;
                GameManager.inventory.RemoveItem(ID2);
            }
        }

        if (GameManager.GetInvItem(resultID) != null)
        {
            GameManager.GetInvItem(resultID).picked = true;
            GameManager.inventory.AddItem(resultID);
        }
    }

}
