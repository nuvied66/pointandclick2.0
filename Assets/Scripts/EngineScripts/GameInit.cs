﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GameInit : MonoBehaviour
{

    // Use this for initialization.
    public GameObject gameManager;
    //public GameObject actionManager;

    [HideInInspector]
    public List<GameObject> _menu;

    public UIManager uiManager;

    void Awake()
    {
        References refs = (References)Resources.Load("References");

        uiManager = refs.uiManager;
        
        if (GameManager.i == null)
        {
            GameObject go = GameObject.Instantiate(gameManager);
            go.name = "GameManager";
        }
//        if (GameObject.FindGameObjectWithTag("GameManager") == null)
//        {
//            GameObject go = GameObject.Instantiate(gameManager);
//            go.name = go.tag;
//            
//        }

        InitMenu();
    }

    void Start()
    {
        CameraFade.StartAlphaFade(Color.black, true, .5f, OnLevelLoad);

    }

    void OnLevelLoad()
    {
        Debug.Log("Level is loaded");
    }

    void InitMenu()
    {
        GameObject.Instantiate(uiManager.cursor);
        for (int i = 0; i < uiManager.gUI.Count; i++)
        {
			
            GameObject go = GameObject.Instantiate(uiManager.gUI[i].canvas.gameObject) as GameObject;
            go.name = uiManager.gUI[i].name;
            _menu.Add(go);
            GameManager.menuObject = _menu;
            if (!uiManager.gUI[i].enableOnStart)
                go.GetComponent<Canvas>().enabled = false;
		
        }
        //GameManager.GetMenu("Speech").GetComponent<Canvas>().enabled = false;
        //GameManager.runtimeInventory = GameManager.dataInv;
        Application.targetFrameRate = 40;

        GameManager.runtimeInventory = GameManager.i.GetComponent<Inventory>().items;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            GameManager.SceneSwitch(0);
            SaveSystem.Save();
        }
    }
}
