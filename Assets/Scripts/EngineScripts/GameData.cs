﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


[System.Serializable]
public class GameData
{

    public List <BoolGlobalVars> bools;
    public List <FloatGlobalVars> floats;
    public List <IntGlobalVars> ints;
    public List <Item> inv;
    public List <Item> dataInv;
    public GlobalVaribalManager.GameStates states;
    public string curretScene;

   
	
}
