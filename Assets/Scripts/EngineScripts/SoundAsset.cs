﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[CreateAssetMenu(menuName = "Point n Click/Sound")]
public class SoundAsset : ScriptableObject
{

    public AudioSource asource;
    public AudioClip aclip;


}
