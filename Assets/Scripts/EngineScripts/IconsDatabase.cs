﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Point n Click/Icons Data")]
public class IconsDatabase : ScriptableObject {

    public List<Icon> icons;

    public void RemoveIcon(int id)
    {
        for (int i = 0; i < icons.Count; i++)
        {
            if (icons[i].id == id)
            {
                icons.Remove(icons[i]);
                //Debug.Log("Remove");
                return;
            }
        }
    }

    public Sprite AssignIcon(Item item)
    {
        for (int i = 0; i < icons.Count; i++)
        {
            if (icons[i].id == item.ID)
            {
                return icons[i].icon;
            }
        }
        return null;
    }
}
