﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ShowHide : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler {

	// Use this for initialization
    bool show = false;
    bool animating = false;
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        if (animating)
            return;

        Interact();

    }


    void FlipArrow()
    {
        if(show)
            transform.GetChild(0).transform.DORotate(new Vector3(0,0,0), .2f);
        else
            transform.GetChild(0).transform.DORotate(new Vector3(0, 0, 180), .2f);
        animating = false;
    }

    public void Interact()
    {
        animating = true;

        if (show)
            transform.parent.DOMoveY(-49, .25f).OnComplete(FlipArrow);
        else
            transform.parent.DOMoveY(49, .25f).OnComplete(FlipArrow);

        show = !show;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (GameManager.currentItem != null)
            return;

        if (Input.GetMouseButton(0))
            Interact();
    }
}
