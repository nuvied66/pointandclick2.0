﻿using UnityEngine;
using System.Collections;

public class AddInv : Interaction
{

    public string[] itemname;

    public override void Interact()
    {
        foreach (string s in itemname)
        {
            GameManager.inventory.AddItem(s);
            GameManager.slotItem = GameManager.GetInvItem(s);
            GameManager.GetMenu("InvButton").GetComponentInChildren<SelectedSlotScript>().UpdateSlot(GameManager.slotItem);
            GameManager.GetInvItem(s).picked = true;
            GameManager.GetInvItem(s).used = false;

            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
           
            print("Added to inv...");
        }
    }
}
