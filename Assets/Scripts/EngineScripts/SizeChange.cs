﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SizeChange : MonoBehaviour {

	// Use this for initialization
    static volatile SizeChange instance = null;

    public GameObject slot;
	void Start () {
        instance = this;
        //UpateInventory();
        InitInventory();
	
	}

    public void UpateInventory()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject s = transform.GetChild(i).gameObject;
            s.GetComponent<Image>().enabled = false;

        }
        
        Vector2 size = GetComponent<RectTransform>().sizeDelta;


        //GameManager.runtimeInventory = GameManager.dataInv;
        int idx = 0;
        int inc = 98;
        foreach (Item i in GameManager.runtimeInventory)
        {
            GameObject s = transform.GetChild(idx).gameObject;
            size = new Vector2(inc, 98);
            GetComponent<RectTransform>().sizeDelta = size;
            s.GetComponent<Image>().sprite = i.icon;
            s.GetComponent<testSlot>().slotItem = i;
            s.GetComponent<Image>().enabled = true;
            inc += 98;
            idx++;



        }
    }

    public void InitInventory()
    {

        Vector2 size = GetComponent<RectTransform>().sizeDelta;


        //GameManager.runtimeInventory = GameManager.dataInv;

        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject s = transform.GetChild(i).gameObject;
            s.name = "Slot number :" + i;
            s.GetComponent<Image>().enabled = false;

        }

        int inc = 98;
        int idx = 0;
        foreach (Item i in GameManager.runtimeInventory)
        {
            GameObject s = transform.GetChild(idx).gameObject;
            size = new Vector2(inc, 98);
            GetComponent<RectTransform>().sizeDelta = size;
            s.GetComponent<Image>().enabled = true;
            s.GetComponent<Image>().sprite = i.icon;
            s.GetComponent<testSlot>().slotItem = i;
            inc += 98;
            idx++;


        }
    }

     

    public static SizeChange Instance { get { return instance; } }
	
	// Update is called once per frame
	void Update () {
	
	}
}
