﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.EventSystems;


public class GameManager : MonoBehaviour
{
    

    public States dStates;
    public static States state;
    public int width, height;
    // this is a singletone persistant class
   
    
    static GameManager manager;
    ///////////////////////////////////////////
    /// 
    /// 
    /// 
    public static PlayerDatabase playerData;
    public static Player currentPlayer;
    public static ActionManager actionManger; 
    //public GlobalVaribalManager variableManager;

    public static VariableManager varManager;

    public static List<GameObject> menuObject;

    public static Inventory inventory;
    // runtime inv
    public static List<Item> runtimeInventory;
    // data inv
    public static List<Item> dataInv;

    public List<Icon> iconDb;
    // global vars
    public static Item currentItem, slotItem;

    public static float fadeTime = .5f;

    public static UIManager uiManger;

    //public UIManager ui;

    public static float letterPause = .01f;

    //static List<char> letter;
    


    ////////////////////////////////	/// <summary>
    /// 


    //public static GlobalVaribalManager VariableManager { get { return GameManager.i.variableManager; } }

    //public static VariableManager VarManager { get { return GameManager.i.varManager; } }

    //public static ActionManager ActionMan{get{ return GameManager.i.}}

    public static GameManager i { get { return manager; } }


    void Awake()
    {
        //CameraFade.StartAlphaFade (Color.black, true, fadeTime);
        //print ("Scene start");
        References refs = (References)Resources.Load("References");
        if (refs != null)
        {
            playerData = refs.playerData;
            uiManger = refs.uiManager;
            varManager = refs.varManager;
            dataInv = refs.inventoryManager.items;
            iconDb = refs.inventoryManager.iconDB.icons;
            GetComponent<Inventory>().dataInv = refs.inventoryManager;
            GetComponent<Inventory>().iconDB = refs.inventoryManager.iconDB;
            
        }
        else
        {
            Debug.LogError("Please Make Refernece Manager");
        }

        Screen.SetResolution(width, height, true);
        if (manager == null)
        {
            manager = this;
            DontDestroyOnLoad(this);
        }
        else if (manager != this)
        {
            Destroy(gameObject);
        }
		
        //Callback = SceneManager.LoadScene (0);
        actionManger = GetComponent<ActionManager>();
        runtimeInventory = GetComponent<Inventory>().items;
        
        //
        //i = this;
        inventory = GetComponent<Inventory>();
        
        currentPlayer = playerData.GetActivePlayer();
       // uiManger = ui;
		
        ////////////////////////
        //InitMenu ();
        /// //////////////////
        /// 

    }

    void Start()
    {
        CopyInv(GameManager.dataInv);
        CopyInv(GameManager.runtimeInventory);
        

        for (int i = 0; i < dataInv.Count; i++)
        {

            if ((dataInv[i].picked && dataInv[i].used == false) || dataInv[i].carryOnStart)
            {

                runtimeInventory.Add(dataInv[i]);

            }
        }
       
    }


    public static void PlaySpeech(string text, bool typein = true)
    {
        if (text.Length <= 0 || IsOn(GetMenu("Speech")))
            return;

        GameManager.state = States.Paused;
        GameManager.manager.StopAllCoroutines();
        GameManager.manager.CancelInvoke();
        //letter.Clear ();
        GetMenu("Speech").GetComponentInChildren<UnityEngine.UI.Text>().text = "";
        TurnOnMenu(GetMenu("Speech"));

        //GameObject.FindGameObjectWithTag ("Dialogue").GetComponentInChildren<UnityEngine.UI.Text> ().text = "";
        //GameObject.FindGameObjectWithTag ("Dialogue").GetComponent<Canvas> ().enabled = true;
        GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosX(-87.3f, .15f);
        if (!typein)
        {
			
            GetMenu("Speech").GetComponentInChildren<UnityEngine.UI.Text>().text = text;
        }
        else
        {
            GameManager.manager.StartCoroutine(TypeText(text));
        }
        GameManager.manager.Invoke("DisableSpeechUI", text.Length / 5f);
    }

    public void DisableSpeechUI()
    {
        //GetMenu("Speech").transform.GetChild(0).GetComponent<RectTransform>().DOAnchorPosX(-806f, .15f);
        GameManager.manager.StopAllCoroutines();
        TurnOffMenu(GetMenu("Speech"));
        GameManager.state = States.Normal;
    }

    public static Item GetInvItem(int ID)
    {
        for (int i = 0; i < dataInv.Count; i++)
        {
            if (dataInv[i].ID == ID)
                return dataInv[i];
        }
        return null;
    }

    public static Item GetInvItem(string name)
    {
        for (int i = 0; i < dataInv.Count; i++)
        {
            if (dataInv[i].name == name)
                return dataInv[i];
        }
        return null;
    }


    //    void Update()
    //    {
    //        if (!EventSystem.current.IsPointerOverGameObject())
    //        {
    //            if (Input.GetMouseButtonDown(0))
    //            {
    //                Invoke("DeselectItem", .1f);
    //            }
    //        }
    //    }

    void LateUpdate()
    {
        if (Input.GetMouseButtonDown(0) && IsOn(GetMenu("Speech")))
            TurnOffMenu(GetMenu("Speech"));

        if (!Input.GetMouseButton(0))
        {
            currentItem = null;
            CursorUI.DisbleCursor();
        }


        dStates = GameManager.state;
//<<<<<<< HEAD
        //print(GameManager.state);
//=======
        //print(GameManager.currentItem);
//>>>>>>> 8dfe5d44c218952dbdc129de38c3126bb214100c
    }

    //    void DeselectItem()
    //    {
    //        currentItem = null;
    //        CursorUI.DisbleCursor();
    //    }



    static IEnumerator TypeText(string message)
    {
//		foreach (char letter in message.ToCharArray()) {
//			GetMenu ("Speech").GetComponentInChildren<UnityEngine.UI.Text> ().text += letter;
        char[] letter = message.ToCharArray();

        for (int i = 0; i < message.Length; i++)
        {
            GetMenu("Speech").GetComponentInChildren<UnityEngine.UI.Text>().text += letter[i];

		


            yield return new WaitForSeconds(letterPause);
        }
		     
    }




    public static GameObject GetMenu(string name)
    {
        
        if (menuObject == null || menuObject.Count < 1)
            return null;
		
        for (int i = 0; i < menuObject.Count; i++)
        {
            if (menuObject[i].name == name)
                return menuObject[i].gameObject;

        }

        Debug.LogWarning("No menu found with name " + name);
        return null;
    }


    public static void TurnOnMenu(GameObject menu, float time = .25f)
    {
        // check here if menu is already on
        if (menu.GetComponent<Canvas>().enabled)
            return;
		
        menu.GetComponent<CanvasGroup>().alpha = 0;
        menu.GetComponent<Canvas>().enabled = true;
        GameManager.manager.StartCoroutine((FadeInMenu(menu, time)));
    }

    public static void TurnOnMenu(string menuname, float time = .25f)
    {
        // check here if menu is already on
        GameObject menu = GetMenu(menuname);

        if (menu.GetComponent<Canvas>().enabled)
            return;

        menu.GetComponent<CanvasGroup>().alpha = 0;
        menu.GetComponent<Canvas>().enabled = true;
        GameManager.manager.StartCoroutine((FadeInMenu(menu, time)));
    }

    static IEnumerator FadeInMenu(GameObject menu, float time)
    {
        float distance = Math.Abs(menu.GetComponent<CanvasGroup>().alpha - 1);
        while (menu.GetComponent<CanvasGroup>().alpha < 1f)
        {
            menu.GetComponent<CanvasGroup>().alpha += distance / (time * 40);
            yield return new WaitForEndOfFrame();

        }

    }



    public static void TurnOffMenu(GameObject menu, float time = .25f)
    {
        // check here if menu is already off

        if (!menu.GetComponent<Canvas>().enabled)
            return;

        GameManager.state = States.Normal;
        menu.GetComponent<CanvasGroup>().alpha = 1;
        GameManager.manager.StartCoroutine((FadeOutMenu(menu, time)));
    }

    public static void TurnOffMenu(string menuname, float time = .25f)
    {
        // check here if menu is already off
        GameObject menu = GetMenu(menuname);
        if (!menu.GetComponent<Canvas>().enabled)
            return;

        GameManager.state = States.Normal;
        menu.GetComponent<CanvasGroup>().alpha = 1;
        GameManager.manager.StartCoroutine((FadeOutMenu(menu, time)));
    }

    static IEnumerator FadeOutMenu(GameObject menu, float time)
    {
        float distance = Math.Abs(menu.GetComponent<CanvasGroup>().alpha - 0);
        while (menu.GetComponent<CanvasGroup>().alpha > 0f)
        {
            menu.GetComponent<CanvasGroup>().alpha -= distance / (time * 40);
            //print (distance / (time * 40));
            yield return new WaitForEndOfFrame();

        }
        menu.GetComponent<Canvas>().enabled = false;

    }

    public static bool IsOn(GameObject menu)
    {
        if (menu.GetComponent<Canvas>().enabled)
            return true;
        else
            return false;
    }

    public static void ToggleMenu(GameObject menu)
    {
        GameManager.manager.StopAllCoroutines();
        if (IsOn(menu))
            TurnOffMenu(menu);
        else
            TurnOnMenu(menu);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="scene switch is obsolute Use ActionManager.sceneswitch"></param>

    public static void SceneSwitch(int index)
    {
        Debug.LogError("scene switch is obsolute Use Action Manager");
        return;
        //OnFinishFade callback = GameManager.i.LoadLevel (index);
        //callback (index);
        //CameraFade.StartAlphaFade(Color.black, false, 1f);
        //GameManager.manager.StartCoroutine(LoadLevel(index));
        //GameManager.i.LoadLevel (index);
        //GameManager.state = States.Paused;
    }

    public static void SceneSwitch(string name)
    {
        Debug.LogError("scene switch is obsolute Use Action Manager");
        return;
       // CameraFade.StartAlphaFade(Color.black, false, 1f);
        //GameManager.manager.StartCoroutine(LoadLevel(name));
       // GameManager.state = States.Paused;
    }

    static IEnumerator LoadLevel(int idx)
    {
		

        yield return new WaitForSeconds(1f);
        GameManager.state = States.Normal;
        SceneManager.LoadScene(idx);
    }

    static IEnumerator LoadLevel(string name)
    {


        yield return new WaitForSeconds(1f);
        GameManager.state = States.Normal;
        SceneManager.LoadScene(name);
    }

    public Menu Menu(string name)
    {
        for (int i = 0; i < uiManger.gUI.Count; i++)
        {
            if (uiManger.gUI[i].name == name)
                return uiManger.gUI[i];
        }
        return null;
    }

    public void OnApplicationQuit()
    {
        
    }

    public void CopyInv(List<Item> inv)
    {
        foreach (Item i in inv)
        {
            for (int x = 0; x < iconDb.Count; x++)
            {
                if (i.ID == iconDb[x].id)
                {
                    i.icon = iconDb[x].icon;
                    break;
                }
            }
            
        }
    }
	





}
