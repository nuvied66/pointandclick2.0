﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SaveSystem
{

    // Use this for initialization
    public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/save.sav", FileMode.OpenOrCreate);

        GameData data = new GameData();
        //data.bools = GameManager.VariableManager.boolVars;
        data.dataInv = GameManager.dataInv;
        data.inv = GameManager.i.GetComponent<Inventory>().items;
        data.curretScene = SceneManager.GetActiveScene().name;
        bf.Serialize(file, data);
        file.Close();

    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/save.sav"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/save.sav", FileMode.Open);
            GameData data = (GameData)bf.Deserialize(file);
            file.Close();

            GameManager.i.GetComponent<Inventory>().items = data.inv;
            GameManager.dataInv = data.dataInv;
            //GameManager.VariableManager.boolVars = data.bools;
            GameManager.SceneSwitch(data.curretScene);
            GameManager.i.CopyInv(GameManager.dataInv);
            GameManager.i.CopyInv(GameManager.i.GetComponent<Inventory>().items);

        }
        else
            GameManager.SceneSwitch(1);

    }

    public static void DeleteSave()
    {
        if (File.Exists(Application.persistentDataPath + "/save.sav"))
        {
            File.Delete(Application.persistentDataPath + "/save.sav");
            Debug.Log("fileDelete");
        }

        //GlobalVaribalManager gVars = GameManager.VariableManager;
        List<Item> iDB = GameManager.dataInv;
        foreach (Item i in iDB)
        {

            i.picked = false;
            i.used = false;
        }

//        foreach (BoolGlobalVars v in gVars.boolVars)
//        {
//            v.value = false;
//        }

        PlayerPrefs.SetString("sceneName", "BoatScene");  
    }

}
