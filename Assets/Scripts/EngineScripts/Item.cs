﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item
{
    public string name, discription;
    public int ID, combineID, resultID;
    public string combineDiscription;

    [System.NonSerialized]
    public Sprite icon;
    public bool picked = false;
    public bool used = false;
    public bool carryOnStart = false;

    public int[] splitIDs;
    public string splitDiscription;
    public bool retainAfterCombine = false;

    [System.NonSerialized]
    public bool isEditing;
    public bool canCombine;

    public Item()
    {
    }
    public Item(string n, string dis, int id, int combineid, int resultid, Sprite sp)
    {
        name = n;
        discription = dis;
        ID = id;
        combineID = combineid;
        resultID = resultID;
        icon = sp;
    }
}