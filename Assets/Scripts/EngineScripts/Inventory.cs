﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{

    // Use this for initialization
    public List<Item> items;

   [HideInInspector] public InvDatabase dataInv;
   [HideInInspector] public IconsDatabase iconDB;



    public void AddItem(int ID)
    {
//        if (!GameManager.runtimeInventory.Contains(GameManager.GetInvItem(ID)))
//        {
//            return;
//        }

       
        items.Add(dataInv.GetItemByID(ID));
        GameManager.runtimeInventory = items;
        //GameManager.GetMenu("Inventory").transform.GetChild(0).GetComponent<InvSlots>().UpdateInventory();
        SizeChange.Instance.UpateInventory();
        print("Item Added");
    }

    public void AddItem(string name)
    {

//        if (GameManager.runtimeInventory.Contains(GameManager.GetInvItem(name)))
//        {
//            return;
//        }

        items.Add(dataInv.GetItemByName(name));
        GameManager.runtimeInventory = items;
       // GameManager.GetMenu("Inventory").transform.GetChild(0).GetComponent<InvSlots>().UpdateInventory();
        SizeChange.Instance.UpateInventory();
        print("Item Added");
    }

    void Awake()
    {
        //dataInv = Resources.Load<InvDatabase> ("InvDatabase");
        //dataInv = 
        //dataInv = 
        // use line below to copy all database items in runtime inv
        //items = dataInv.items;
    }

    public void RemoveItem(int ID)
    {
        items.Remove(dataInv.GetItemByID(ID));
        GameManager.runtimeInventory = items;
        //GameManager.GetMenu("Inventory").transform.GetChild(0).GetComponent<InvSlots>().UpdateInventory();
        SizeChange.Instance.UpateInventory();
        GameManager.slotItem = null;
        SelectedSlotScript.EmptySlot();
        
        
    }

    public void RemoveItem(string name)
    {
        items.Remove(dataInv.GetItemByName(name));
        GameManager.runtimeInventory = items;
        //GameManager.GetMenu("Inventory").transform.GetChild(0).GetComponent<InvSlots>().UpdateInventory();
        SizeChange.Instance.UpateInventory();
        GameManager.slotItem = null;
        SelectedSlotScript.EmptySlot();
    }

    public void ReplaceItem(int ID1, int ID2)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].ID == ID1)
            {

                // GameManager.GetInvItem(ID1).picked = true;
                items[i] = dataInv.GetItemByID(ID2);

                //print(items[i].name);

                items[i].used = true;

                //////////////
                items[i] = dataInv.GetItemByID(ID2);

                items[i].picked = true;
                ///////////////
 
                GameManager.runtimeInventory = items;
                //GameManager.GetMenu("Inventory").transform.GetChild(0).GetComponent<InvSlots>().UpdateInventory();
                SizeChange.Instance.UpateInventory();
                break;
            }
        }
    }

    public void AddItemsByID(int[] IDs)
    {
        for (int i = 0; i < IDs.Length; i++)
        {
            items.Add(dataInv.GetItemByID(IDs[i]));
            GameManager.runtimeInventory = items;
            //GameManager.GetMenu("Inventory").transform.GetChild(0).GetComponent<InvSlots>().UpdateInventory();
            SizeChange.Instance.UpateInventory();
        }
    }

	

}
