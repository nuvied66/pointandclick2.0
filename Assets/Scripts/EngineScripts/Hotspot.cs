﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider2D))]
public class Hotspot : MonoBehaviour
{
    public Interaction defaultInt, itemInt;
    public Interaction randomInt;
    public bool discardOnUse;
    public string useableItem;
    public bool hasItemInt;
    public int itemIdx;
    //public References refs;


    void Awake()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        //refs = (References)Resources.Load("References");
    }

    public void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;		
    }

    public void Update()
    {
        if (GameManager.state == States.Paused)
            return;
        
        if (onMouseUp() || OnTouchDown())
        {
            if (GameManager.currentItem == null)
            {
                //print(GameManager.currentItem);
                defaultInt.Interact();
                return;
            }

            if (GameManager.currentItem.ID == GameManager.dataInv[itemIdx].ID)
            {


                if (discardOnUse)
                {
                    GameManager.inventory.RemoveItem(GameManager.currentItem.ID);
                    print(GameManager.dataInv[itemIdx].name);
                }

                itemInt.Interact();
                return;
            }
            else
            {
                RandomDialoge();
                return;
                
            }		
        }

    }

    private void RandomDialoge()
    {
        GameManager.actionManger.PlaySpeech("This Item can't work here....", false);
    }

    public bool OnTouchDown()
    {
        if (Input.touchCount > 0)
        if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            return false;
		
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            Vector2 touchPos = Input.GetTouch(0).position;
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touchPos), Vector2.zero);
            if (hit.collider == GetComponent<Collider2D>())
            {

                return true;
            }
						
        }
        return false;

    }

    public bool onMouseUp()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return false;
		
        if (Input.GetMouseButtonUp(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider == GetComponent<Collider2D>())
            {
                return true;
            } 
        }
        return false;
    }

    public void TurnOff()
    {
        gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        //gameObject.GetComponent<BoxCollider2D>().enabled = false;
    }

    public void TurnOn()
    {

        gameObject.layer = LayerMask.NameToLayer("Default");
    }
}
