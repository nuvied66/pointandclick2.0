﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectedSlotScript : MonoBehaviour, IBeginDragHandler,IDragHandler, IPointerDownHandler
{
    public Item item = null;
    static SelectedSlotScript i;

    #region IDragHandler implementation

    public void OnDrag(PointerEventData eventData)
    {
        

    }

    #region IPointerDownHandler implementation

    public void OnPointerDown(PointerEventData eventData)
    {
        GameManager.currentItem = item;
        CursorScript.EnableCursor();
        CursorUI.EnableCursor();
    }

    #endregion

    #endregion

    #region IBeginDragHandler implementation

    public void OnBeginDrag(PointerEventData eventData)
    {
        //GameManager.currentItem = item;
        //CursorScript.EnableCursor ();
        //CursorUI.EnableCursor();

    }

    #endregion




    // Use this for initialization

    //print (eventData.pointerDrag);
 




    void Start()
    {
        i = this;
        
        if (GameManager.slotItem == null)
        {
            GetComponent<Image>().enabled = false;
            return;
        }
        item = GameManager.slotItem;
        GetComponent<Image>().enabled = true;
        GetComponent<Image>().sprite = item.icon;

    }

    public void UpdateSlot(Item i)
    {
        GetComponent<Image>().enabled = true;
        GetComponent<Image>().sprite = i.icon;
        item = i;
        GameManager.slotItem = i;
        // GameManager.currentItem = item;
    }

    public void ShiftRight()
    {
        if (GameManager.runtimeInventory.Count > 0)
        {
            int idx = GameManager.runtimeInventory.IndexOf(item);
            //print(idx);
            if (idx > -1)
            {
                if (idx == GameManager.runtimeInventory.Count - 1)
                {
                    item = GameManager.runtimeInventory[0];
                    UpdateSlot(item);
                }
                else
                {
                    item = GameManager.runtimeInventory[idx + 1];
                    UpdateSlot(item);
                }
            }
            else
            {
                item = GameManager.runtimeInventory[0];
                UpdateSlot(item);
            }
        }
    }

    public void ShiftLeft()
    {
        if (GameManager.runtimeInventory.Count > 0)
        {
            int idx = GameManager.runtimeInventory.IndexOf(item);
            //print(idx);
            if (idx > -1)
            {
                if (idx == 0)
                {
                    item = GameManager.runtimeInventory[GameManager.runtimeInventory.Count - 1];
                    UpdateSlot(item);
                }
                else
                {
                    item = GameManager.runtimeInventory[idx - 1];
                    UpdateSlot(item);
                }
            }
            else
            {
                item = GameManager.runtimeInventory[0];
                UpdateSlot(item);
            }
        }
    }

    public static void EmptySlot()
    {
        if (i == null)
            return;
        i.GetComponent<Image>().enabled = false;
        i.item = null;
    }

    public static SelectedSlotScript instance{ get { return i; } }
}
