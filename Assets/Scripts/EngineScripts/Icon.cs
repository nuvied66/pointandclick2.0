﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Icon
{
    public string name;
    public int id;
    public Sprite icon;
}
