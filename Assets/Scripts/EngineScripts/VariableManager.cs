﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Point n Click/new Varibale Manager")]
public class VariableManager : ScriptableObject {

    public List<Variable> vars;



#if UNITY_EDITOR
    string nameFilter = "";
    Vector2 scrollPos = new Vector2(0,0);
    Variable selectedVar;

    public void ShowGui()
    {
        //GUILayout.Label("Variable Editor", EditorStyles.largeLabel);

        nameFilter = EditorGUILayout.TextField("Search : ?", nameFilter);

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Height(Mathf.Min(vars.Count * 21, 235f) + 5));

        foreach (Variable v in vars.ToArray())
        {
            if (v.name.ToLower().Contains(nameFilter.ToLower()) || nameFilter == "")
            {
                GUILayout.BeginHorizontal();
                if (v.name == "")
                {
                    v.name = "new"+v.type.ToString()+" "+ v.ID;
                }
                if (GUILayout.Toggle(v.isEditing, v.name + " : " + v.ID, "toolbarbutton"))
                {
                    if (selectedVar != v)
                    {
                        DeactivateAllItems();
                        v.isEditing = true;
                        selectedVar = v;
                    }
                }

                if (GUILayout.Button("Delete", GUILayout.Width(100f), GUILayout.Height(15f)))
                {
                    Undo.RecordObject(this, "delete item");
                    vars.RemoveAt(vars.IndexOf(v));
                    DeactivateAllItems();
                }
                GUILayout.EndHorizontal();

            }
        }

        EditorGUILayout.EndScrollView();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Variable"))
        {
            Variable v = new Variable();
            v.name = "New Variable";
            if (vars.Count > 0)
                v.ID = vars[vars.Count - 1].ID + 1;
            else
                v.ID = 0;

            vars.Add(v);
            selectedVar = v;
        }
        EditorGUILayout.EndHorizontal();

        VariableGUI();
        //Debug.Log(index+ "," +resultIdx);
        if (GUI.changed)
        {
            EditorUtility.SetDirty(this);
        }
    }

    private void VariableGUI()
    {
        if (selectedVar == null || !vars.Contains(selectedVar))
            return;
        selectedVar.name = EditorGUILayout.TextField("Variable Name :", selectedVar.name);
        selectedVar.type = (VarType)EditorGUILayout.EnumPopup("Type of Variable :", selectedVar.type);

        switch (selectedVar.type)
        {
            case VarType.bools:
                selectedVar.boolValue = EditorGUILayout.Toggle("Value :",selectedVar.boolValue);
                break;
            case VarType.floats:
                selectedVar.floatValue = EditorGUILayout.FloatField("Value :", selectedVar.floatValue);
                break;
            case VarType.strings:
                selectedVar.stringValue = EditorGUILayout.TextField("Value :", selectedVar.stringValue);
                break;
            case VarType.ints:
                selectedVar.intValue = EditorGUILayout.IntField("Value :", selectedVar.intValue);
                break;
            default:
                break;
        }
    }

    private void DeactivateAllItems()
    {
        foreach (Variable v in vars)
        {
            v.isEditing = false;
        }
        selectedVar = null;
    }
#endif


    public void SetBool(int id, bool value)
    {
        
        for(int i = 0; i < vars.Count; i++)
        {

            if(vars[i].ID == id)
            {
                vars[i].boolValue = value;
                return;
            }
        }
    }

    public void SetBool(string name, bool value)
    {

        for (int i = 0; i < vars.Count; i++)
        {

            if (vars[i].name == name)
            {
                vars[i].boolValue = value;
                return;
            }
        }
    }

    public void SetInt(int id, int value)
    {

        for (int i = 0; i < vars.Count; i++)
        {

            if (vars[i].ID == id)
            {
                vars[i].intValue = value;
                return;
            }
        }
    }

    public void SetInt(string name, int value)
    {

        for (int i = 0; i < vars.Count; i++)
        {

            if (vars[i].name == name)
            {
                vars[i].intValue = value;
                return;
            }
        }
    }

    public void SetFloat(string name, float value)
    {

        for (int i = 0; i < vars.Count; i++)
        {

            if (vars[i].name == name)
            {
                vars[i].floatValue = value;
                return;
            }
        }
    }

    public void SetFloat(int id, float value)
    {

        for (int i = 0; i < vars.Count; i++)
        {
            if (vars[i].ID == id)
            {
                vars[i].floatValue = value;
                return;
            }
        }
    }

    public void SetString(int id, string value)
    {

        for (int i = 0; i < vars.Count; i++)
        {
            
            if (vars[i].ID == id)
            {
                vars[i].stringValue = value;
                return;
            }
        }
    }

    public void SetString(string name, string value)
    {

        for (int i = 0; i < vars.Count; i++)
        {
            
            if (vars[i].name == name)
            {
                vars[i].stringValue = value;
                return;
            }
        }
    }
    ///////////////////////////////////////////////////////////////

    public bool GetBool(int id)
    {
        for (int i = 0; i < vars.Count; i++)
        {
            if (vars[i].ID == id)
                return vars[i].boolValue;
        }
        return false;

    }

    public bool GetBool(string name)
    {
        for (int i = 0; i < vars.Count; i++)
        {
            if (vars[i].name == name)
                return vars[i].boolValue;
        }
        return false;

    }

    public string GetString(string name)
    {
        for (int i = 0; i < vars.Count; i++)
        {
            if (vars[i].name == name)
                return vars[i].stringValue;
        }
        return "";

    }

    public string GetString(int id)
    {
        for (int i = 0; i < vars.Count; i++)
        {
            if (vars[i].ID == id)
                return vars[i].stringValue;
        }
        return "";

    }
}
