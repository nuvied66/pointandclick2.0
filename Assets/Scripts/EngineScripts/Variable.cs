﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Variable {

    

    public string name;
    public int ID;
    public VarType type;

    public int intValue;
    public bool boolValue;
    public float floatValue;
    public string stringValue;

    public bool isEditing;
    /*
    public void SetValue(bool val)
    {
        if (type != Type.bools)
            return;
        boolValue = val;
    }
    public void SetValue(int val)
    {
        if (type != Type.ints)
            return;
        intValue = val;
    }
    public void SetValue(float val)
    {
        if (type != Type.floats)
            return;
        floatValue = val;
    }
    public void SetValue(string val)
    {
        if (type != Type.strings)
            return;
        stringValue = val;
    }*/


}
