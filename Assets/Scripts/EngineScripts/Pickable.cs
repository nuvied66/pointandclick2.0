﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using DG.Tweening;


[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(AudioSource))]
public class Pickable : MonoBehaviour
{
    public Interaction[] I;
    public string itemname;

    void Awake()
    {
        //GetComponent<AudioSource>().playOnAwake = false;
    }

    void Start()
    {
       
        if (GameManager.GetInvItem(itemname).picked)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;
        }		
    }

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;	

        GetComponent<BoxCollider2D>().enabled = false;
        transform.DOScale(transform.localScale * 1.75f, .5f);
        GetComponent<SpriteRenderer>().DOFade(0, .5f);
        GameManager.inventory.AddItem(itemname);
        GameManager.slotItem = GameManager.GetInvItem(itemname);
        GameManager.GetMenu("InvButton").GetComponentInChildren<SelectedSlotScript>().UpdateSlot(GameManager.slotItem);
        GameManager.GetInvItem(itemname).picked = true;
        GameManager.slotItem = GameManager.GetInvItem(itemname);
        SelectedSlotScript.instance.UpdateSlot(GameManager.GetInvItem(itemname));
        Interact();
        //print("Woking");

    }

    void Interact()
    {
        GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);

        Invoke("RunInteractions", .5f);
    }

    void RunInteractions()
    {
        if (I != null)
        {
            foreach (Interaction i in I)
            {
            
                i.Interact();
            }
        }
    }
}
