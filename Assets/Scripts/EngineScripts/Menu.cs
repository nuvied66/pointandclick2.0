﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Menu : ScriptableObject
{
    public string name;
    public Canvas canvas;
    public bool enableOnStart;
    public bool isOn;
    public int ID;
    public MenuType type;
    public bool isEditing;
    GameObject gameobject;
    

	
    void OnEnable()
    {
        if(canvas != null)
            gameobject = canvas.gameObject;
    }

    public GameObject gameObject{ get { return gameobject; } }


    internal virtual void ShowGUI()
    {
        
    }


}
