﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CloseHintUI : MonoBehaviour, IPointerClickHandler {

	// Use this for initialization

    public void OnPointerClick(PointerEventData eventData)
    {
        GameManager.TurnOffMenu(GameManager.GetMenu("HintUI"));
    }
}
