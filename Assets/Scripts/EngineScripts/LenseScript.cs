﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LenseScript : MonoBehaviour, IDropHandler
{
    //public Text exText;


    public void Examine()
    {
        if (GameManager.currentItem.splitIDs.Length > 0)
        {
            GameManager.PlaySpeech(GameManager.currentItem.splitDiscription);
            Split(GameManager.currentItem.splitIDs, GameManager.currentItem.ID);
            GameManager.currentItem = null;

        }
        else
        {
            //GameManager.PlaySpeech (GameManager.currentItem.discription);
            GameManager.actionManger.PlaySpeech(GameManager.currentItem.discription, false);
        }
    }

    public void Split(int[] IDs, int targetID)
    {

        GameManager.inventory.RemoveItem(targetID);
        GameManager.inventory.AddItemsByID(IDs);


    }


    public void OnDrop(PointerEventData eventData)
    {
        Examine();
    }
}
