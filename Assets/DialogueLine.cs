﻿using UnityEngine;
using System.Collections;

public class DialogueLine : MonoBehaviour {

	// Use this for initialization
    public UnityEngine.UI.Text text;
    public Color textColor;
    void Awake()
    {

        text = GetComponent<UnityEngine.UI.Text>();
        text.color = textColor;
    }
	// Update is called once per frame
	void Update () {
	
	}
}
