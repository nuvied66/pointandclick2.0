﻿using UnityEngine;
using System.Collections;

public class PlayerImage : MonoBehaviour {

    public UnityEngine.UI.Image playerImg;

    void Awake()
    {
        playerImg = GetComponent<UnityEngine.UI.Image>();
    }
}
