# README #

## Point and Click 2.0 ##

### Features ###
1. Built-in Inventory with combine and split.
2. Easy set up of a simple game Escape and Point and Click.

### Dependencies ###
  Actions of this framework are built on DoTween.
  
### How it works ###
1. Create a Reference Manager from Asset/create/pointnclick
2. Create 4 managers required by references, and assign them in references manager.
3. Now you have 4 managers, Ui Manager, Inventory Manager, Variable Manager, Player Database.
4. Now you need 1 more manager name Icon Manager, create that.
5. Icon manager is needed to assign in the Inventory Manager.
6. Now Select Game Manager Prefab and assign required managers.
7. Now You need to assign Game Manager prefab in the Game Init prefab.
8. You need to place Game Init in every scene.

#### UI Manager ####
You don't need to set manager yourself, Use MenuEditor instead (PointnClick/MenuEditor). 
2 Menus are essential,
1. "Speech" must have same name and speech type.
2. "Inventory" should have same name and Inv type.

#### Inventory Editor ####
You don't need to set manager yourself, Use MenuEditor instead (PointnClick/InventoryEditor).

#### Action Manager ####

Action manager handles all of the actions that you perform. Every action's last argument is a system.Action or Tween.callback (A simple non argument base action will fit)

Example:

public void CameraAction(CameraActionType type, float _duration = .25f, Action OnFinish = null)

public void CameraAction(CameraActionType type, Action OnFinish = null)

public void CameraSwitch(GameCamera _target, float _duration, TweenCallback OnFinish = null)
